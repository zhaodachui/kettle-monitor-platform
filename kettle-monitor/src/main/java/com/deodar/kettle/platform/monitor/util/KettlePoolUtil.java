package com.deodar.kettle.platform.monitor.util;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author: Administrator
 * @Date: 2020/04/15 16:02
 * @Description:
 * @version:1.0.0
 */
@Slf4j
public class KettlePoolUtil {

    private static ExecutorService executorService = null; //并行任务池

    private static volatile KettlePoolUtil instance = null;

    private  KettlePoolUtil(){}

    public static KettlePoolUtil getInstance(){
        if(instance == null){
            synchronized (KettlePoolUtil.class){
                instance = new KettlePoolUtil();
                //从系统配置项或者数据库中查询默认线程池大小
                executorService = Executors.newFixedThreadPool(6);
            }
        }
        return instance;
    }

    public Future<?> run(Callable callable){
      return   executorService.submit(callable);
    }

    public Future<?> run(Runnable runnable){
      return   executorService.submit(runnable);
    }

    public void shutdwon(){
        int size = TaskManageUtil.getInstance().getSize();
        if(size >0){
            log.info("线程池中还有["+size+"]条任务在运行，无法关闭线程池");
            return;
        }
        executorService.shutdown();

    }

    public void newTheadPool(int threadNum){
        int size = TaskManageUtil.getInstance().getSize();
        if(size >0){
            log.info("线程池中还有["+size+"]条任务在运行，无法关闭线程池,创建新的线程池");
            return;
        }
        executorService.shutdown();

        if(threadNum >= 20 || threadNum<=0 ) {
            executorService = Executors.newFixedThreadPool(6);
        }else {
            executorService = Executors.newFixedThreadPool(threadNum);
        }
    }

}
