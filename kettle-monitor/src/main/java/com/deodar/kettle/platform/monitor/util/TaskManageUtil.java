package com.deodar.kettle.platform.monitor.util;

import java.util.Hashtable;

/**
 * @author: Administrator
 * @Description:
 * @version:1.0.0
 */
public class TaskManageUtil {

    private static volatile TaskManageUtil instance = null;
    private static Hashtable<String,Object> hashtable = new Hashtable<>();

    public  static TaskManageUtil getInstance(){
        if(instance == null){
            synchronized (TaskManageUtil.class){
                if(instance == null){
                    instance = new TaskManageUtil();
                }
            }
        }
        return instance;
    }

    public void add(String key,Object obj){
        hashtable.put(key,obj);
    }

    public  Object get(String key){
       return hashtable.get(key);
    }

    public void remove(String key){
        hashtable.remove(key);
    }

    public int  getSize(){
        return  hashtable.size();
    }

    private  TaskManageUtil(){}
}
