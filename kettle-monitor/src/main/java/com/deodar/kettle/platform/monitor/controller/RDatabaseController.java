package com.deodar.kettle.platform.monitor.controller;

import com.deodar.kettle.platform.database.domain.RDatabase;
import com.deodar.kettle.platform.database.service.IRDatabaseService;
import com.deodar.common.annotation.Log;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.core.page.TableDataInfo;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.utils.poi.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 
 * @author Administrator
 * @date 2020-04-08
 */
@Controller
@RequestMapping("/system/database")
public class RDatabaseController extends BaseController {
    private String prefix = "system/database";

    @Autowired
    private IRDatabaseService rDatabaseService;

    @RequiresPermissions("system:database:view")
    @GetMapping()
    public String database()
    {
        return prefix + "/database";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:database:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RDatabase rDatabase)
    {
        startPage();
        List<RDatabase> list = rDatabaseService.selectRDatabaseList(rDatabase);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:database:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RDatabase rDatabase)
    {
        List<RDatabase> list = rDatabaseService.selectRDatabaseList(rDatabase);
        ExcelUtil<RDatabase> util = new ExcelUtil<RDatabase>(RDatabase.class);
        return util.exportExcel(list, "database");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("system:database:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RDatabase rDatabase)
    {
        return toAjax(rDatabaseService.insertRDatabase(rDatabase));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idDatabase}")
    public String edit(@PathVariable("idDatabase") Long idDatabase, ModelMap mmap)
    {
        RDatabase rDatabase = rDatabaseService.selectRDatabaseById(idDatabase);
        mmap.put("rDatabase", rDatabase);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("system:database:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RDatabase rDatabase)
    {
        return toAjax(rDatabaseService.updateRDatabase(rDatabase));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:database:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(rDatabaseService.deleteRDatabaseByIds(ids));
    }
}
