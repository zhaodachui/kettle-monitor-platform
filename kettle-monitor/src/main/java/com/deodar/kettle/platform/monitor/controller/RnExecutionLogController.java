package com.deodar.kettle.platform.monitor.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import com.deodar.kettle.platform.database.domain.RnExecutionLog;
import com.deodar.kettle.platform.database.domain.RnStepStatus;
import com.deodar.kettle.platform.database.service.IRnExecutionLogService;
import com.deodar.kettle.platform.database.service.IRnStepStatusService;
import com.deodar.kettle.platform.monitor.service.JobService;
import com.deodar.kettle.platform.monitor.service.TransService;
import com.deodar.kettle.platform.monitor.util.KettleConfigUtil;
import lombok.SneakyThrows;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.deodar.common.annotation.Log;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletResponse;

/**
 * 任务执行日志Controller
 * 
 * @author ruoyi
 * @date 2020-04-26
 */
@Controller
@RequestMapping("/kettle/executeLog")
public class RnExecutionLogController extends BaseController {
    private String prefix = "kettle/executeLog";

    @Autowired
    private IRnExecutionLogService rnExecutionLogService;

    @Autowired
    private IRnStepStatusService rnStepStatusService;

    @Autowired
    @Qualifier("transService")
    private TransService transService;

    @Autowired
    @Qualifier("jobService")
    private JobService jobService;


    @RequiresPermissions("kettle:executeLog:view")
    @GetMapping()
    public String executeLog() {

        return prefix + "/executeLog";
    }

    /**
     * 查询任务执行日志列表
     */
    @RequiresPermissions("kettle:executeLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RnExecutionLog rnExecutionLog) {
        startPage();
        List<RnExecutionLog> list = rnExecutionLogService.selectRnExecutionLogList(rnExecutionLog);
        return getDataTable(list);
    }

    /**
     * 导出任务执行日志列表
     */
    @RequiresPermissions("kettle:executeLog:export")
    @Log(title = "任务执行日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RnExecutionLog rnExecutionLog) {
        List<RnExecutionLog> list = rnExecutionLogService.selectRnExecutionLogList(rnExecutionLog);
        ExcelUtil<RnExecutionLog> util = new ExcelUtil<RnExecutionLog>(RnExecutionLog.class);
        return util.exportExcel(list, "executeLog");
    }

    /**
     * 新增任务执行日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务执行日志
     */
    @RequiresPermissions("kettle:executeLog:add")
    @Log(title = "任务执行日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RnExecutionLog rnExecutionLog) {
        return toAjax(rnExecutionLogService.insertRnExecutionLog(rnExecutionLog));
    }

    /**
     * 修改任务执行日志
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        RnExecutionLog rnExecutionLog = rnExecutionLogService.selectRnExecutionLogById(id);
        mmap.put("rnExecutionLog", rnExecutionLog);
        return prefix + "/edit";
    }

    /**
     * 修改任务执行日志
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        RnExecutionLog rnExecutionLog = rnExecutionLogService.selectRnExecutionLogById(id);
        mmap.put("rnExecutionLog", rnExecutionLog);
        return prefix + "/stepStatus/stepStatus";
    }

    @RequiresPermissions("kettle:executeLog:list")
    @PostMapping("/stepStatus/list")
    @ResponseBody
    public TableDataInfo stepStatusList(String taskId) {
        startPage();
        List<RnStepStatus> list = rnStepStatusService.selectRnStepStatusList(new RnStepStatus(taskId));

        return getDataTable(list);
    }

    /**
     * 修改保存任务执行日志
     */
    @RequiresPermissions("kettle:executeLog:edit")
    @Log(title = "任务执行日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RnExecutionLog rnExecutionLog) {
        return toAjax(rnExecutionLogService.updateRnExecutionLog(rnExecutionLog));
    }

    /**
     * 删除任务执行日志
     */
    @RequiresPermissions("kettle:executeLog:remove")
    @Log(title = "任务执行日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(rnExecutionLogService.deleteRnExecutionLogByIds(ids));
    }


    //@RequiresPermissions("kettle:transformation:execute")
    @Log(title = "停止转换", businessType = BusinessType.FORCE)
    @GetMapping(value = "/stop/{id}")
    @ResponseBody
    public AjaxResult stop(@PathVariable("id")String id){
        RnExecutionLog rnExecutionLog = rnExecutionLogService.selectRnExecutionLogById(id);
        if( 1 == rnExecutionLog.getTaskType()){
            return toAjax(transService.stop(id));
        }else {
            return toAjax(jobService.stop(id));
        }

    }


    /**
     * 修改保存任务执行日志
     */
    @SneakyThrows
    @RequiresPermissions("kettle:executeLog:list")
    @Log(title = "任务执行图片", businessType = BusinessType.OTHER)
    @GetMapping("/image")
    @ResponseBody
    public void KettleImage(@RequestParam String id, HttpServletResponse response) {
        if(id !=null){
            RnExecutionLog rnExecutionLog = rnExecutionLogService.selectRnExecutionLogById(id);
            String kettleImagePath = KettleConfigUtil.getKettleImagePath();
            String path = kettleImagePath+"/trans/"+ rnExecutionLog.getImagePath();
            if(2 == rnExecutionLog.getTaskType()){
                path = kettleImagePath+"/job/"+ rnExecutionLog.getImagePath();
            }

            File file = new File(path);

            FileInputStream fis;
            fis = new FileInputStream(file);
            long size = file.length();
            byte[] temp = new byte[(int) size];
            fis.read(temp, 0, (int) size);
            fis.close();
            byte[] data = temp;
            response.setContentType("image/png");
            OutputStream out = response.getOutputStream();
            out.write(data);
            out.flush();
            out.close();
        }


    }



}
