package com.deodar.kettle.platform.monitor.controller;

import com.deodar.common.core.controller.BaseController;
import com.deodar.kettle.platform.database.domain.RnExecutionLog;
import com.deodar.kettle.platform.database.service.IRnExecutionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: kettle-data-platform
 * @Description: kettle监控任务主页
 * @author: Administrator
 * @version: 1.0.0
 * @Date: 2020-04-07 09:23
 **/

@Controller
@RequestMapping("/kettle/index")
public class KettleIndexController extends BaseController {

    private String prefix = "kettle/index";

    @Autowired
    private IRnExecutionLogService rnExecutionLogService;

    @GetMapping("/index")
    public String index( ModelMap mmap) {
        mmap.put("taskStatistics",rnExecutionLogService.selectTaskGroup(new RnExecutionLog()));

        return prefix + "/kettleIndex";
    }

    @PostMapping("/task")
    @ResponseBody
    public   Map<String,List<Map<String, Object>>> getTaskData(){
        List<Map<String, Object>> trans = rnExecutionLogService.selectPieData(new RnExecutionLog(1));
        List<Map<String, Object>> jobs = rnExecutionLogService.selectPieData(new RnExecutionLog(2));
        Map<String,List<Map<String, Object>>> maps  = new HashMap<>();
        maps.put("trans",trans);
        maps.put("job",jobs);
        return maps;
    }

    @GetMapping("/job")
    @ResponseBody
    public List getJob(){
        List<Map<String, Object>> maps = rnExecutionLogService.selectPieData(new RnExecutionLog(2));
        return maps;
    }




}
