package com.deodar.kettle.platform.monitor.controller;

import com.deodar.kettle.platform.database.domain.RJob;
import com.deodar.kettle.platform.database.service.IRJobService;
import com.deodar.kettle.platform.monitor.service.JobService;
import com.deodar.common.annotation.Log;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.core.page.TableDataInfo;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.quartz.domain.SysJob;
import com.deodar.quartz.service.ISysJobService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 * 
 * @author Administrator
 * @date 2020-04-08
 */
@Controller
@RequestMapping("/kettle/job")
public class RJobController extends BaseController {
    private String prefix = "kettle/job";

    @Autowired
    private IRJobService rJobService;


    @Autowired
    @Qualifier("jobService")
    private JobService jobService;


    @Autowired
    private ISysJobService sysJobService;

    @RequiresPermissions("kettle:job:view")
    @GetMapping()
    public String job()
    {
        return prefix + "/job";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:job:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RJob rJob) {
        startPage();
        List<RJob> list = rJobService.selectRJobList(rJob);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:job:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RJob rJob) {
        List<RJob> list = rJobService.selectRJobList(rJob);
        ExcelUtil<RJob> util = new ExcelUtil<RJob>(RJob.class);
        return util.exportExcel(list, "job");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:job:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RJob rJob)
    {
        return toAjax(rJobService.insertRJob(rJob));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap) {
        RJob rJob = rJobService.selectRJobById(id);
        mmap.put("rJob", rJob);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:job:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RJob rJob)
    {
        return toAjax(rJobService.updateRJob(rJob));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("kettle:job:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(rJobService.deleteRJobByIds(ids));
    }


    @RequiresPermissions("kettle:job:execute")
    @Log(title = "执行任务", businessType = BusinessType.EXECUTE)
    @GetMapping("/start/{id}")
    @ResponseBody
    public AjaxResult start(@PathVariable("id") Integer id){
        boolean flag = jobService.start(id);
        return toAjax(flag);
    }

    @RequiresPermissions("kettle:job:execute")
    @Log(title = "停止任务", businessType = BusinessType.FORCE)
    @GetMapping(value = "/stop/{logId}")
    @ResponseBody
    public AjaxResult stop(@PathVariable("logId")String logId){
        return toAjax(jobService.stop(logId));
    }

    /**
     * 修改调度
     */
    @RequiresPermissions("kettle:job:execute")
    @GetMapping("/editQuartz/{id}")
    public String editQuartz(@PathVariable("id") Integer taskId, ModelMap mmap) {
        SysJob sysJob = sysJobService.selectJobByTaskId(taskId);
        if(sysJob == null){
            sysJob = new SysJob();
            RJob rJob = rJobService.selectRJobById(taskId);
            sysJob.setJobName(rJob.getName());
            sysJob.setInvokeTarget("jobService.start("+taskId+")");
            sysJob.setTaskId(taskId);
        }
        mmap.put("job", sysJob);
        return  "/monitor/job/edit";
    }

}
