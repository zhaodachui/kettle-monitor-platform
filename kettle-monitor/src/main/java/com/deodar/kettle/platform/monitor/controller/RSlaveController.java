package com.deodar.kettle.platform.monitor.controller;

import com.deodar.kettle.platform.common.util.JSONObject;
import com.deodar.kettle.platform.common.util.KettleEncr;
import com.deodar.kettle.platform.database.domain.RSlave;
import com.deodar.kettle.platform.database.service.IRSlaveService;
import com.deodar.common.annotation.Log;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.core.page.TableDataInfo;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.kettle.platform.monitor.service.SlaveService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Controller
@RequestMapping("/kettle/slave")
public class RSlaveController extends BaseController {
    private String prefix = "kettle/slave";

    @Autowired
    private IRSlaveService rSlaveService;

    @Autowired
    @Qualifier("slaveService")
    private SlaveService slaveService;

    @RequiresPermissions("kettle:slave:view")
    @GetMapping()
    public String slave() {
        return prefix + "/slave";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:slave:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RSlave rSlave) {
        startPage();
        List<RSlave> list = rSlaveService.selectRSlaveList(rSlave);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:slave:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RSlave rSlave)
    {
        List<RSlave> list = rSlaveService.selectRSlaveList(rSlave);
        ExcelUtil<RSlave> util = new ExcelUtil<RSlave>(RSlave.class);
        return util.exportExcel(list, "slave");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:slave:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RSlave rSlave)
    {
        return toAjax(rSlaveService.insertRSlave(rSlave));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap)
    {
        RSlave rSlave = rSlaveService.selectRSlaveById(id);
        rSlave.setPassword(KettleEncr.decryptPasswd(rSlave.getPassword()));
        mmap.put("rSlave", rSlave);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:slave:edit")
    @Log(title = "编辑节点", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RSlave rSlave) {

        return toAjax(rSlaveService.updateRSlave(rSlave));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("kettle:slave:remove")
    @Log(title = "删除节点", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(rSlaveService.deleteRSlaveByIds(ids));
    }

    @RequiresPermissions("kettle:slave:edit")
    @Log(title = "测试节点", businessType = BusinessType.OTHER)
    @GetMapping("/testSlave/{id}")
    @ResponseBody
    public JSONObject testSlave(@PathVariable("id") Integer id){
        try {
            JSONObject jsonObject = slaveService.testSlave(id);
            return jsonObject;
        } catch (Exception e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("carteStatus","N");
            return jsonObject;
        }

    }
}
