package com.deodar.kettle.platform.monitor.controller;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RDirectory;
import com.deodar.kettle.platform.database.service.IRDirectoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.deodar.common.annotation.Log;
import com.deodar.common.enums.BusinessType;

import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.common.utils.StringUtils;
import com.deodar.common.core.domain.Ztree;

/**
 * 
 * @author Administrator
 * @date 2020-04-21
 */
@Controller
@RequestMapping("/kettle/directory")
public class RDirectoryController extends BaseController {
    private String prefix = "kettle/directory";

    @Autowired
    private IRDirectoryService rDirectoryService;

    @RequiresPermissions("kettle:directory:view")
    @GetMapping()
    public String directory()
    {
        return prefix + "/directory";
    }

    /**
     * 查询目录分类树列表
     */
    @RequiresPermissions("kettle:directory:list")
    @PostMapping("/list")
    @ResponseBody
    public List<RDirectory> list(RDirectory rDirectory) {
        List<RDirectory> list = rDirectoryService.selectRDirectoryList(rDirectory);
        return list;
    }

    /**
     * 导出目录分类列表
     */
    @RequiresPermissions("kettle:directory:export")
    @Log(title = "目录分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RDirectory rDirectory) {
        List<RDirectory> list = rDirectoryService.selectRDirectoryList(rDirectory);
        ExcelUtil<RDirectory> util = new ExcelUtil<RDirectory>(RDirectory.class);
        return util.exportExcel(list, "directory");
    }

    /**
     * 新增目录分类
     */
    @GetMapping(value = { "/add/{idDirectory}", "/add/" })
    public String add(@PathVariable(value = "idDirectory", required = false) Integer idDirectory, ModelMap mmap) {
        if (StringUtils.isNotNull(idDirectory)) {
            mmap.put("rDirectory", rDirectoryService.selectRDirectoryById(idDirectory));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存目录分类
     */
    @RequiresPermissions("kettle:directory:add")
    @Log(title = "目录分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RDirectory rDirectory) {
        return toAjax(rDirectoryService.insertRDirectory(rDirectory));
    }

    /**
     * 修改目录分类
     */
    @GetMapping("/edit/{idDirectory}")
    public String edit(@PathVariable("idDirectory") Integer idDirectory, ModelMap mmap) {
        RDirectory rDirectory = rDirectoryService.selectRDirectoryById(idDirectory);
        mmap.put("rDirectory", rDirectory);
        return prefix + "/edit";
    }

    /**
     * 修改保存目录分类
     */
    @RequiresPermissions("kettle:directory:edit")
    @Log(title = "目录分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RDirectory rDirectory) {
        return toAjax(rDirectoryService.updateRDirectory(rDirectory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("kettle:directory:remove")
    @Log(title = "目录分类", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{idDirectory}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("idDirectory") Integer idDirectory) {
        return toAjax(rDirectoryService.deleteRDirectoryById(idDirectory));
    }

    /**
     * 选择目录分类树
     */
    @GetMapping(value = { "/selectDirectoryTree/{idDirectory}", "/selectDirectoryTree/" })
    public String selectDirectoryTree(@PathVariable(value = "idDirectory", required = false) Integer idDirectory, ModelMap mmap) {
        if (StringUtils.isNotNull(idDirectory)) {
            mmap.put("rDirectory", rDirectoryService.selectRDirectoryById(idDirectory));
        }
        return prefix + "/tree";
    }

    /**
     * 加载目录分类树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData(@RequestParam Integer type) {
        RDirectory rDirectory = new RDirectory();
        rDirectory.setType(type);
        List<Ztree> ztrees = rDirectoryService.selectRDirectoryTree(rDirectory);
        return ztrees;
    }
}
