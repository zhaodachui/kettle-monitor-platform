package com.deodar.kettle.platform.monitor.controller;

import com.deodar.kettle.platform.database.domain.RLog;
import com.deodar.kettle.platform.database.service.IRLogService;
import com.deodar.common.annotation.Log;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.core.page.TableDataInfo;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.utils.poi.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Controller
@RequestMapping("/kettle/log")
public class RLogController extends BaseController {
    private String prefix = "kettle/log";

    @Autowired
    private IRLogService rLogService;

    @RequiresPermissions("kettle:log:view")
    @GetMapping()
    public String log() {
        return prefix + "/log";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:log:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RLog rLog) {
        startPage();
        List<RLog> list = rLogService.selectRLogList(rLog);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:log:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RLog rLog)
    {
        List<RLog> list = rLogService.selectRLogList(rLog);
        ExcelUtil<RLog> util = new ExcelUtil<RLog>(RLog.class);
        return util.exportExcel(list, "log");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:log:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RLog rLog) {
        return toAjax(rLogService.insertRLog(rLog));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idLog}")
    public String edit(@PathVariable("idLog") Long idLog, ModelMap mmap) {
        RLog rLog = rLogService.selectRLogById(idLog);
        mmap.put("rLog", rLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:log:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RLog rLog)
    {
        return toAjax(rLogService.updateRLog(rLog));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("kettle:log:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(rLogService.deleteRLogByIds(ids));
    }
}
