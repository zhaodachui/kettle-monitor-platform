package com.deodar.kettle.platform.monitor.controller;

import com.deodar.kettle.platform.database.domain.RTransformation;
import com.deodar.kettle.platform.database.service.IRTransformationService;
import com.deodar.kettle.platform.monitor.service.TransService;
import com.deodar.common.annotation.Log;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.core.page.TableDataInfo;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.quartz.domain.SysJob;
import com.deodar.quartz.service.ISysJobService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 * 
 * @author Administrator
 * @date 2020-04-08
 */
@Controller
@RequestMapping("/kettle/transformation")
public class RTransformationController extends BaseController {
    private String prefix = "kettle/transformation";

    @Autowired
    private IRTransformationService rTransformationService;

    @Autowired
    @Qualifier("transService")
    TransService transService;

    @Autowired
    private ISysJobService jobService;

    @RequiresPermissions("kettle:transformation:view")
    @GetMapping()
    public String transformation() {
        return prefix + "/transformation";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:transformation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RTransformation rTransformation) {
        startPage();
        List<RTransformation> list = rTransformationService.selectRTransformationList(rTransformation);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("kettle:transformation:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RTransformation rTransformation)
    {
        List<RTransformation> list = rTransformationService.selectRTransformationList(rTransformation);
        ExcelUtil<RTransformation> util = new ExcelUtil<RTransformation>(RTransformation.class);
        return util.exportExcel(list, "transformation");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:transformation:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RTransformation rTransformation)
    {
        return toAjax(rTransformationService.insertRTransformation(rTransformation));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap) {
        RTransformation rTransformation = rTransformationService.selectRTransformationById(id);
        mmap.put("rTransformation", rTransformation);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("kettle:transformation:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RTransformation rTransformation) {

        return toAjax(rTransformationService.updateRTransformation(rTransformation));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("kettle:transformation:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(rTransformationService.deleteRTransformationByIds(ids));
    }


    @RequiresPermissions("kettle:transformation:execute")
    @Log(title = "执行转换", businessType = BusinessType.EXECUTE)
    @GetMapping(value = "/start/{id}")
    @ResponseBody
    public AjaxResult start(@PathVariable("id") Integer id){
        return toAjax(transService.start(id));
    }

    @RequiresPermissions("kettle:transformation:execute")
    @Log(title = "停止转换", businessType = BusinessType.FORCE)
    @GetMapping(value = "/stop/{logId}")
    @ResponseBody
    public AjaxResult stop(@PathVariable("logId")String logId){
        return toAjax(transService.stop(logId));
    }


    /**
     * 修改调度
     */
    @RequiresPermissions("kettle:transformation:execute")
    @GetMapping("/editQuartz/{id}")
    public String editQuartz(@PathVariable("id") Integer taskId, ModelMap mmap) {
        SysJob sysJob = jobService.selectJobByTaskId(taskId);
        if(sysJob == null){
            sysJob = new SysJob();
            RTransformation rTransformation = rTransformationService.selectRTransformationById(taskId);
            sysJob.setJobName(rTransformation.getName());
            sysJob.setInvokeTarget("transService.start("+taskId+")");
            sysJob.setTaskId(taskId);
        }
        mmap.put("job", sysJob);
        return  "/monitor/job/edit";
    }




}
