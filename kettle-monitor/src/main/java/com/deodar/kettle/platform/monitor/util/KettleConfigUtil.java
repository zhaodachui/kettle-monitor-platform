package com.deodar.kettle.platform.monitor.util;

import com.deodar.common.constant.Constants;
import com.deodar.common.utils.CacheUtils;
import com.deodar.common.utils.StringUtils;


/**
 * @program: kettle-data-platform
 * @Description:
 * @author: Administrator
 * @version: 1.0.0
 * @Date: 2020-04-10 17:54
 **/

public class KettleConfigUtil {

    /**
     * 获取kettle存放图片的路径
     *
     * @param key 参数键
     * @return dictDatas 字典数据列表
     */
    public static String getKettleValue(String key) {
        Object cacheObj = CacheUtils.get(getCacheName(), getCacheKey(key));
        if (StringUtils.isNotNull(cacheObj)) {

            return cacheObj.toString();
        }
        return "/opt/kettle_image";
    }

    public static String getKettleImagePath() {
       return  getKettleValue("kettle.image.path");
    }

    /**
     * 清空缓存
     */
    public static void clearKettleCache() {
        CacheUtils.removeAll(getCacheName());
    }

    /**
     * 获取cache name
     *
     * @return 缓存名
     */
    public static String getCacheName() {
        return Constants.SYS_CONFIG_CACHE;
    }

    /**
     * 设置cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    public static String getCacheKey(String configKey) {
        return Constants.SYS_CONFIG_KEY + configKey;
    }
}
