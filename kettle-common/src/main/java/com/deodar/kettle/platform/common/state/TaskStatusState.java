package com.deodar.kettle.platform.common.state;

/**
 * @author: Administrator
 * @Date: 2019/10/24 08:50
 * @Description:
 * @version:1.0.0
 */
public enum TaskStatusState {

    STATE_UN_KNOW ( -1,"未知状态"),

    STATE_INIT ( 0,"等待执行"),

    STATE_WAITING ( 1,"等待执行"),

    STATE_RUNNING ( 2,"正在执行"),

    STATE_SUCCESS ( 3,"执行成功"),

    STATE_FAIL ( 4,"执行失败"),

    STATE_CLOSED ( 5,"用户关闭"),

    STATE_CHECK_ERROR ( 6,"校验错误"),

    STATE_PARTSUCCESS ( 31,"任务部分执行成功");

    int  status;

    String desc;

    TaskStatusState(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public static TaskStatusState getTaskStatus(int  status){

        for(TaskStatusState taskStatus:values()){
            if(taskStatus.status == status){
                return  taskStatus;
            }
        }

        return  STATE_UN_KNOW;
    }

    public int getStatus() {
        return status;
    }
}
