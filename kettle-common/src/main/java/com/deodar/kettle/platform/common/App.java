package com.deodar.kettle.platform.common;

import com.deodar.kettle.platform.common.config.KettleConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.RowMetaAndData;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.logging.*;
import org.pentaho.di.repository.LongObjectId;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepositoryMeta;
import org.pentaho.metastore.stores.delegate.DelegatingMetaStore;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;

@Slf4j
@Component
@Order(1000)
public class App implements CommandLineRunner {

	@Autowired
	@Qualifier("kettleConfig")
	KettleConfig kettleConfig;

	private static App app;
	public static KettleDatabaseRepositoryMeta meta;

	@Override
	public void run(String... strings) throws Exception {
		setApplicationContext();
	}

	private App() {

        try {
            KettleLogStore.init(5000, 720);
            KettleEnvironment.init();

        } catch (KettleException e) {
            e.printStackTrace();
        }


	}
	


	public static App getInstance() {
		if (app == null) {
			app = new App();
		}
		return app;
	}

	private Repository repository;

	public Repository getRepository() {
		if(repository == null){
			try {
				this.repository = buildRepository();
			} catch (KettleException e) {
				log.warn("无法获取资源库连接:{}",e.getMessage());
			}
		}
		return repository;
	}
	
	private Repository defaultRepository;
	

	
	public Repository getDefaultRepository() {
		return this.defaultRepository;
	}
	
	public void selectRepository(Repository repo) {
		if(repository != null) {
			repository.disconnect();
		}
		repository = repo;
	}

	private DelegatingMetaStore metaStore;

	public DelegatingMetaStore getMetaStore() {
		return metaStore;
	}
	

	
	private RowMetaAndData variables = null;
	private ArrayList<String> arguments = new ArrayList<String>();
	
	public String[] getArguments() {
		return arguments.toArray(new String[arguments.size()]);
	}

	
	public RowMetaAndData getVariables() {
		return variables;
	}

//	@Override
	public void  setApplicationContext() throws BeansException {

		try {

			//DatabaseMeta(String name, String type, String access, String host, String db, String port, String user, String pass)
			this.repository = buildRepository();
			this.app = this;
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}
		
	}

	private KettleDatabaseRepository buildRepository() throws KettleException {
		KettleDatabaseRepository repository = new KettleDatabaseRepository();
		String name = kettleConfig.kettleRepositoryName;
		if(StringUtils.isEmpty(name)) {
			name = kettleConfig.ip + "_" + kettleConfig.dbName;
		}
		DatabaseMeta dbMeta = new DatabaseMeta(name,
				kettleConfig.dbType,"Native",
				kettleConfig.ip,kettleConfig.dbName,kettleConfig.port,
				kettleConfig.userName,kettleConfig.password);

		ObjectId objectId = new LongObjectId(100);
		dbMeta.setObjectId(objectId);
		dbMeta.setShared(true);
		dbMeta.addExtraOption(dbMeta.getPluginId(), "characterEncoding", "utf8");
		dbMeta.addExtraOption(dbMeta.getPluginId(), "useUnicode", "true");
		dbMeta.addExtraOption(dbMeta.getPluginId(), "autoReconnect", "true");
		dbMeta.setVariable("EXTRA_OPTION_MYSQL.useSSL","false");



		meta = new KettleDatabaseRepositoryMeta();
		meta.setName(name);
		meta.setId("KettleDatabaseRepository");
		meta.setConnection(dbMeta);
		meta.setDescription(name);

		repository.init(meta);
		repository.connect(kettleConfig.kettleUserName, kettleConfig.kettlePassword);
		return  repository;
	}

	
}
