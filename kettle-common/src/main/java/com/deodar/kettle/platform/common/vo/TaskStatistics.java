package com.deodar.kettle.platform.common.vo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

/**
 * @program: kettle-data-platform
 * @Description: 任务统计
 * @author: Administrator
 * @version: 1.0.0
 * @Date: 2020-04-07 16:13
 **/
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskStatistics {

    /**
     * 等待任务数
     */
    Integer waitNum;

    /**
     * 执行中任务数
     */
    Integer executingNum;

    /**
     * 成功任务数
     */
    Integer successNum;

    /**
     * 失败数
     */
    Integer failNum;

    /**
     * 强行停止数
     */
    Integer forcedStopNum;

    /**
     * 总共任务数
     */
    Integer totalNum;

    Date updateTime;

}
