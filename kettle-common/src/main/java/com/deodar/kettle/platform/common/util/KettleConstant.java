package com.deodar.kettle.platform.common.util;

/**
 * @author: Administrator
 * @Date: 2019/11/14 14:26
 * @Description:
 * @version:1.0.0
 */
public class KettleConstant {

    public static final String KETLLE_SEPARATOR = "/";

    public static final String KEY_PREFIX = "ks_";
}
