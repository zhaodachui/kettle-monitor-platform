package com.deodar.kettle.platform.common.config;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @program: kettle-data-platform
 * @Description:
 * @author: Administrator
 * @version: 1.0.0
 **/

@Component("kettleConfig")
@FieldDefaults(level = AccessLevel.PUBLIC)
public class KettleConfig {

    @Value("${kettle.repository.ip}")
    String ip;

    @Value("${kettle.repository.port}")
    String port;

    @Value("${kettle.repository.dbname}")
    String dbName;

    @Value("${kettle.repository.dbtype}")
    String dbType;

    @Value("${kettle.repository.username}")
    String userName;

    @Value("${kettle.repository.password}")
    String password;

    @Value("${kettle.repository.driverClassName}")
    String driverClassName;

    @Value("${kettle.repository.kettle-user-name:admin}")
    String kettleUserName;

    @Value("${kettle.repository.kettle-repository-name}")
    String kettleRepositoryName;

    @Value("${kettle.repository.kettle-password:admin}")
    String kettlePassword;





}
