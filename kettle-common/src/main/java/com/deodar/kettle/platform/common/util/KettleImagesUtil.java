package com.deodar.kettle.platform.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.pentaho.di.cluster.SlaveServer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author: Administrator
 * @Date: 2019/5/22 07:20
 * @Description:  获取kettle运行的结果图
 * @version:1.0.0
 */
@Slf4j
public class KettleImagesUtil {


    public static String getImagePath(SlaveServer slave, String type, String taskName, String carteId, String filePath){
        String header = slave.getUsername()+":"+slave.getPassword();
        //获取image信息
        String imgPath = null;
        if("trans".equals(type)){
            String url = "http://"+slave.getHostname()+":"+slave.getPort()+"/kettle/transImage/?name="+taskName+"&id="+carteId;
            imgPath = KettleImagesUtil.getImage(url,header,filePath);
            return  imgPath;
        }else if("job".equals(type)){
            String url = "http://"+slave.getHostname()+":"+slave.getPort()+"/kettle/jobImage/?name="+taskName+"&id="+carteId;
            imgPath = KettleImagesUtil.getImage(url,header,filePath);

        }

        return imgPath;
    }

    public  static String getImage(String httpUrl, String header, String filePath){

        URL url;
        BufferedInputStream in = null;
        FileOutputStream fileOutputStream = null;
        try {
            File file = new File(filePath);
            if(!file.exists()){
                file.mkdirs();
            }
            log.info("获取kettle网络图片");
            String fileName = httpUrl.substring(httpUrl.lastIndexOf("/") + 2);

            fileName = fileName.replaceAll("=","").replaceAll("&","")+".png";
            System.out.println(fileName);
            url = new URL(httpUrl);
            URLConnection connection=url.openConnection();

            connection.setRequestProperty("Authorization","Basic " +  Base64.encodeBase64String(header.getBytes()));

            in = new BufferedInputStream(connection.getInputStream());
            fileOutputStream = new FileOutputStream(new File(filePath+"/"+fileName));
            int t;

            while ((t = in.read()) != -1) {
                fileOutputStream.write(t);

            }
            log.info("图片获取成功");
            return fileName;
        }catch (Exception e) {
            log.error("图片获取失败:"+ExceptionUtils.getStackTrace(e));
            return null;
        }finally {
            if( fileOutputStream !=null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if( in !=null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
