package com.deodar.kettle.platform.common.util;

import lombok.extern.slf4j.Slf4j;
import org.pentaho.di.repository.LongObjectId;
import org.pentaho.di.repository.ObjectId;


@Slf4j
public class  RepositoryUtils {


	public static ObjectId getObjectId(Integer id){
		ObjectId objectId = new LongObjectId(Long.valueOf(String.valueOf(id)));
		return  objectId;
	}


}
