package com.deodar;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


/**
 * 项目启动类
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class})
@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan(basePackages = {"com.deodar.**.mapper"})
@ServletComponentScan(basePackages = {"com.deodar"})
public class KettleMonitorApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(KettleMonitorApplication.class, args);

        String[] beanNames =  ctx.getBeanDefinitionNames();
        System.out.println("项目中beanNames个数："+beanNames.length);

       /* for(String bean:beanNames){

            System.out.println(bean);

        }*/
        System.out.println("-----------------*启动成功*--------------------");

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(KettleMonitorApplication.class);
    }
}