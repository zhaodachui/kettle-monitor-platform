function queryDirectoryFileTree() {
    queryDirTree(1);//查询文件资源库分类树
}


function queryDirectoryTree() {
    queryDirTree(0);//查询数据库资源库分类树
}

function queryDirTree(type) {
    var url = ctx + "kettle/directory/treeData?type="+type;
    var options = {
        url: url,
        expandLevel: 2,
        onClick : zOnClick
    };
    $.tree.init(options);

    function zOnClick(event, treeId, treeNode) {
        $("#deptId").val(treeNode.id);
        $("#parentId").val(treeNode.pId);
        $.table.search();
    }
}

$('#btnExpand').click(function() {
    $._tree.expandAll(true);
    $(this).hide();
    $('#btnCollapse').show();
});

$('#btnCollapse').click(function() {
    $._tree.expandAll(false);
    $(this).hide();
    $('#btnExpand').show();
});

$('#btnRefresh').click(function() {
    queryDirectoryTree();
});
function dept() {
    var url = ctx + "kettle/directory";
    $.modal.openTab("目录管理", url);
}

/*参数组分类树*/
function selectParamGroupTree() {
    var options = {
        title: '参数组值选择',
        width: "380",
        url: ctx + "kettle/group/selectGroupTree/" + $("#treeId").val(),
        callBack: doSubmit
    };
    $.modal.openOptions(options);
}

function doSubmit(index, layero){
    var body = layer.getChildFrame('body', index);
    var d = body.find('#treeId').val();
    console.log("d:"+d)
    $("#parGroupId").val(d);
    $("#treeName").val(body.find('#treeName').val());
    layer.close(index);
}