package com.deodar.kettle.platform.database.service.impl;

import java.util.*;

import com.deodar.kettle.platform.common.vo.TaskStatistics;
import com.deodar.kettle.platform.database.domain.RnExecutionLog;
import com.deodar.kettle.platform.database.domain.RnStepStatus;
import com.deodar.kettle.platform.database.mapper.RnExecutionLogMapper;
import com.deodar.kettle.platform.database.mapper.RnStepStatusMapper;
import com.deodar.kettle.platform.database.service.IRnExecutionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 任务执行日志Service业务层处理
 * 
 * @author
 * @date 2020-04-06
 */
@Service
public class RnExecutionLogServiceImpl implements IRnExecutionLogService {
    @Autowired
    private RnExecutionLogMapper rnExecutionLogMapper;

    @Autowired
    private RnStepStatusMapper rnStepstatusMapper;


    /**
     * 查询任务执行日志
     * 
     * @param id 任务执行日志ID
     * @return 任务执行日志
     */
    @Override
    public RnExecutionLog selectRnExecutionLogById(String id) {
        return rnExecutionLogMapper.selectRnExecutionLogById(id);
    }

    /**
     * 查询任务执行日志列表
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 任务执行日志
     */
    @Override
    public List<RnExecutionLog> selectRnExecutionLogList(RnExecutionLog rnExecutionLog) {
        return rnExecutionLogMapper.selectRnExecutionLogList(rnExecutionLog);
    }

    /**
     * 新增任务执行日志
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 结果
     */
    @Override
    public int insertRnExecutionLog(RnExecutionLog rnExecutionLog) {
        return rnExecutionLogMapper.insertRnExecutionLog(rnExecutionLog);
    }

    /**
     * 修改任务执行日志
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 结果
     */
    @Override
    public int updateRnExecutionLog(RnExecutionLog rnExecutionLog) {
        return rnExecutionLogMapper.updateRnExecutionLog(rnExecutionLog);
    }

    /**
     * 删除任务执行日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRnExecutionLogByIds(String ids) {
        return rnExecutionLogMapper.deleteRnExecutionLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务执行日志信息
     * 
     * @param id 任务执行日志ID
     * @return 结果
     */
    @Override
    public int deleteRnExecutionLogById(String id) {
        return rnExecutionLogMapper.deleteRnExecutionLogById(id);
    }

    @Override
    public void batchInsertStepStatus(List<RnStepStatus> rnStepStatusList) {
        if(rnStepStatusList !=null && rnStepStatusList.size() >0){
            for (RnStepStatus t: rnStepStatusList) {
                rnStepstatusMapper.insertRnStepStatus(t);
            }
        }
    }


    @Override
    public int deleteStepStatus(String id) {
        return rnStepstatusMapper.deleteStepStatusByTaskId(id);
    }

    @Override
    public TaskStatistics selectTaskGroup(RnExecutionLog rnExecutionLog){

        List<Map<String, Object>> mapList = rnExecutionLogMapper.selectTaskGroup(rnExecutionLog);
        TaskStatistics taskStatistics = new TaskStatistics();
        taskStatistics.setUpdateTime(new Date());
        taskStatistics.setTotalNum(0);
        if(mapList !=null && mapList.size() >0 ){

            for(Map<String,Object> map : mapList){

                Integer status = Integer.parseInt(map.get("status").toString());
                Integer dataNum = Integer.parseInt(map.get("data_num").toString());
                taskStatistics.setTotalNum(taskStatistics.getTotalNum()+dataNum);
                switch (status){
                    case 1:
                         taskStatistics.setWaitNum(dataNum);
                         break;
                    case 2:
                        taskStatistics.setExecutingNum(dataNum);
                        break;
                    case 3:
                        taskStatistics.setSuccessNum(dataNum);
                        break;
                    case 4:
                        taskStatistics.setFailNum(dataNum);
                        break;
                    case 5:
                        taskStatistics.setForcedStopNum(dataNum);
                        break;

                }

            }// end for

        }
        return taskStatistics;

    }

    @Override
    public List<Map<String,Object>> selectPieData(RnExecutionLog rnExecutionLog){
        List<Map<String, Object>> mapList = rnExecutionLogMapper.selectTaskGroup(rnExecutionLog);
        List<Map<String, Object>> mapArrayList = new ArrayList<>();
        if(mapList !=null && mapList.size() >0 ){
            for(Map<String,Object> map : mapList){

                Integer status = Integer.parseInt(map.get("status").toString());
                Integer dataNum = Integer.parseInt(map.get("data_num").toString());
                Map<String,Object> objectMap = new HashMap<>();
                objectMap.put("value",dataNum);
                switch (status){
                    case 1:
                        objectMap.put("name","等待执行");
                        break;
                    case 2:
                        objectMap.put("name","正在执行");
                        break;
                    case 3:
                        objectMap.put("name","执行成功");
                        break;
                    case 4:
                        objectMap.put("name","执行失败");
                        break;
                    case 5:
                        objectMap.put("name","强行停止");
                        break;

                }
                mapArrayList.add(objectMap);

            }// end for

        }
        return mapArrayList;
    }


}
