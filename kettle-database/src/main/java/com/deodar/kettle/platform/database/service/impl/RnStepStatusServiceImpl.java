package com.deodar.kettle.platform.database.service.impl;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RnStepStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.kettle.platform.database.mapper.RnStepStatusMapper;
import com.deodar.kettle.platform.database.service.IRnStepStatusService;
import com.deodar.common.core.text.Convert;

/**
 * 任务中间过程Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service
public class RnStepStatusServiceImpl implements IRnStepStatusService
{
    @Autowired
    private RnStepStatusMapper RnStepStatusMapper;

    /**
     * 查询任务中间过程
     * 
     * @param id 任务中间过程ID
     * @return 任务中间过程
     */
    @Override
    public RnStepStatus selectRnStepStatusById(String id)
    {
        return RnStepStatusMapper.selectRnStepStatusById(id);
    }

    /**
     * 查询任务中间过程列表
     * 
     * @param RnStepStatus 任务中间过程
     * @return 任务中间过程
     */
    @Override
    public List<RnStepStatus> selectRnStepStatusList(RnStepStatus RnStepStatus)
    {
        return RnStepStatusMapper.selectRnStepStatusList(RnStepStatus);
    }

    /**
     * 新增任务中间过程
     * 
     * @param RnStepStatus 任务中间过程
     * @return 结果
     */
    @Override
    public int insertRnStepStatus(RnStepStatus RnStepStatus)
    {
        return RnStepStatusMapper.insertRnStepStatus(RnStepStatus);
    }

    /**
     * 修改任务中间过程
     * 
     * @param RnStepStatus 任务中间过程
     * @return 结果
     */
    @Override
    public int updateRnStepStatus(RnStepStatus RnStepStatus)
    {
        return RnStepStatusMapper.updateRnStepStatus(RnStepStatus);
    }

    /**
     * 删除任务中间过程对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRnStepStatusByIds(String ids)
    {
        return RnStepStatusMapper.deleteRnStepStatusByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务中间过程信息
     * 
     * @param id 任务中间过程ID
     * @return 结果
     */
    @Override
    public int deleteRnStepStatusById(String id)
    {
        return RnStepStatusMapper.deleteRnStepStatusById(id);
    }
}
