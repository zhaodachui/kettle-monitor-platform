package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 任务执行日志对象 rn_execution_log
 * 
 * @author ruoyi
 * @date 2020-04-26
 */
public class RnExecutionLog extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 日志id */
    @Excel(name = "日志id")
    private String id;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 执行方式 */
    @Excel(name = "执行方式")
    private String executionMethod;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 配置 */
    @Excel(name = "配置")
    private String executionConfiguration;

    /** 任务日志详细信息 */
    @Excel(name = "任务日志详细信息")
    private String executionLog;

    /** 执行方式 1:串行，2:并行 */
    @Excel(name = "执行方式 1:串行，2:并行")
    private Integer executionType;

    /** 任务组 */
    @Excel(name = "任务组")
    private String taskGroup;

    /** null */
    @Excel(name = "null")
    private String carteId;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imagePath;

    /** 任务id */
    @Excel(name = "任务id")
    private Integer taskId;

    /** 排序 */
    @Excel(name = "排序")
    private Integer taskSort;

    /** 批次 */
    @Excel(name = "批次")
    private String batchId;

    /** 任务类型：1:转换 2:job.3:任务组 */
    @Excel(name = "任务类型：1:转换 2:job.3:任务组")
    private Integer taskType;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }
    public void setTaskName(String taskName){
        this.taskName = taskName;
    }

    public String getTaskName(){
        return taskName;
    }
    public void setStartTime(Date startTime){
        this.startTime = startTime;
    }

    public Date getStartTime(){
        return startTime;
    }
    public void setEndTime(Date endTime){
        this.endTime = endTime;
    }

    public Date getEndTime(){
        return endTime;
    }
    public void setExecutionMethod(String executionMethod){
        this.executionMethod = executionMethod;
    }

    public String getExecutionMethod(){
        return executionMethod;
    }
    public void setStatus(Integer status){
        this.status = status;
    }

    public Integer getStatus(){
        return status;
    }
    public void setExecutionConfiguration(String executionConfiguration){
        this.executionConfiguration = executionConfiguration;
    }

    public String getExecutionConfiguration(){
        return executionConfiguration;
    }
    public void setExecutionLog(String executionLog){
        this.executionLog = executionLog;
    }

    public String getExecutionLog(){
        return executionLog;
    }
    public void setExecutionType(Integer executionType){
        this.executionType = executionType;
    }

    public Integer getExecutionType(){
        return executionType;
    }
    public void setTaskGroup(String taskGroup){
        this.taskGroup = taskGroup;
    }

    public String getTaskGroup(){
        return taskGroup;
    }
    public void setCarteId(String carteId){
        this.carteId = carteId;
    }

    public String getCarteId(){
        return carteId;
    }
    public void setImagePath(String imagePath){
        this.imagePath = imagePath;
    }

    public String getImagePath(){
        return imagePath;
    }
    public void setTaskId(Integer taskId){
        this.taskId = taskId;
    }

    public Integer getTaskId(){
        return taskId;
    }
    public void setTaskSort(Integer taskSort){
        this.taskSort = taskSort;
    }

    public Integer getTaskSort(){
        return taskSort;
    }
    public void setBatchId(String batchId){
        this.batchId = batchId;
    }

    public String getBatchId(){
        return batchId;
    }
    public void setTaskType(Integer taskType){
        this.taskType = taskType;
    }

    public Integer getTaskType(){
        return taskType;
    }


    public RnExecutionLog() {
    }

    public RnExecutionLog(Integer taskType) {
        this.taskType = taskType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskName", getTaskName())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("executionMethod", getExecutionMethod())
            .append("status", getStatus())
            .append("executionConfiguration", getExecutionConfiguration())
            .append("executionLog", getExecutionLog())
            .append("executionType", getExecutionType())
            .append("taskGroup", getTaskGroup())
            .append("carteId", getCarteId())
            .append("imagePath", getImagePath())
            .append("taskId", getTaskId())
            .append("taskSort", getTaskSort())
            .append("batchId", getBatchId())
            .append("taskType", getTaskType())
            .append("remark", getRemark())
            .toString();
    }
}
