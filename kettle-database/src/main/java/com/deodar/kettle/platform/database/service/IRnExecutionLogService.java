package com.deodar.kettle.platform.database.service;

import com.deodar.kettle.platform.common.vo.TaskStatistics;
import com.deodar.kettle.platform.database.domain.RnExecutionLog;
import com.deodar.kettle.platform.database.domain.RnStepStatus;

import java.util.List;
import java.util.Map;


/**
 * 任务执行日志Service接口
 * 
 * @author
 * @date 2020-04-06
 */
public interface IRnExecutionLogService {
    /**
     * 查询任务执行日志
     * 
     * @param id 任务执行日志ID
     * @return 任务执行日志
     */
    public RnExecutionLog selectRnExecutionLogById(String id);

    /**
     * 查询任务执行日志列表
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 任务执行日志集合
     */
    public List<RnExecutionLog> selectRnExecutionLogList(RnExecutionLog rnExecutionLog);

    /**
     * 新增任务执行日志
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 结果
     */
    public int insertRnExecutionLog(RnExecutionLog rnExecutionLog);

    /**
     * 修改任务执行日志
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 结果
     */
    public int updateRnExecutionLog(RnExecutionLog rnExecutionLog);

    /**
     * 批量删除任务执行日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRnExecutionLogByIds(String ids);

    /**
     * 删除任务执行日志信息
     * 
     * @param id 任务执行日志ID
     * @return 结果
     */
    public int deleteRnExecutionLogById(String id);

    public void batchInsertStepStatus(List<RnStepStatus> RnStepStatusList);

    /**
     * 查询统计任务数
     * @param rnExecutionLog
     * @return
     */
    TaskStatistics selectTaskGroup(RnExecutionLog rnExecutionLog);

    List<Map<String,Object>> selectPieData(RnExecutionLog rnExecutionLog);

    int deleteStepStatus(String id);
}
