package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RSlave;

import java.util.List;


/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface RSlaveMapper {
    /**
     * 查询【请填写功能名称】
     * 
     * @param id【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RSlave selectRSlaveById(Integer id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rSlave 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RSlave> selectRSlaveList(RSlave rSlave);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rSlave 【请填写功能名称】
     * @return 结果
     */
    public int insertRSlave(RSlave rSlave);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rSlave 【请填写功能名称】
     * @return 结果
     */
    public int updateRSlave(RSlave rSlave);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRSlaveById(Integer id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRSlaveByIds(String[] ids);
}
