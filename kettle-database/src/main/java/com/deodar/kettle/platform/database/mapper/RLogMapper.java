package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RLog;

import java.util.List;


/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface RLogMapper {
    /**
     * 查询【请填写功能名称】
     * 
     * @param idLog 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RLog selectRLogById(Long idLog);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rLog 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RLog> selectRLogList(RLog rLog);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rLog 【请填写功能名称】
     * @return 结果
     */
    public int insertRLog(RLog rLog);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rLog 【请填写功能名称】
     * @return 结果
     */
    public int updateRLog(RLog rLog);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idLog 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRLogById(Long idLog);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idLogs 需要删除的数据ID
     * @return 结果
     */
    public int deleteRLogByIds(String[] idLogs);
}
