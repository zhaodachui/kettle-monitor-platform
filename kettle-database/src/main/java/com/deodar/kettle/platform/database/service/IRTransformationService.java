package com.deodar.kettle.platform.database.service;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RTransformation;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface IRTransformationService {
    /**
     * 查询【请填写功能名称】
     * 
     * @param idTransformation 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RTransformation selectRTransformationById(Integer idTransformation);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RTransformation> selectRTransformationList(RTransformation rTransformation);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 结果
     */
    public int insertRTransformation(RTransformation rTransformation);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 结果
     */
    public int updateRTransformation(RTransformation rTransformation);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRTransformationByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idTransformation 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRTransformationById(Integer idTransformation);
}
