package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 【请填写功能名称】对象 r_transformation
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RTransformation extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer idDirectory;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String description;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extendedDescription;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String transVersion;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long transStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idStepRead;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idStepWrite;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idStepInput;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idStepOutput;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idStepUpdate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDatabaseLog;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tableNameLog;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer useBatchid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer useLogfield;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDatabaseMaxdate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tableNameMaxdate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fieldNameMaxdate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long offsetMaxdate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long diffMaxdate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String createdUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String modifiedUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date modifiedDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sizeRowset;

    private String parGroupId;

    private String parGroupName;

    private Long deptId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDirectory() {
        return idDirectory;
    }

    public void setIdDirectory(Integer idDirectory) {
        this.idDirectory = idDirectory;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParGroupId() {
        return parGroupId;
    }

    public void setParGroupId(String parGroupId) {
        this.parGroupId = parGroupId;
    }

    public String getName()
    {
        return name;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setExtendedDescription(String extendedDescription) 
    {
        this.extendedDescription = extendedDescription;
    }

    public String getExtendedDescription() 
    {
        return extendedDescription;
    }
    public void setTransVersion(String transVersion) 
    {
        this.transVersion = transVersion;
    }

    public String getTransVersion() 
    {
        return transVersion;
    }
    public void setTransStatus(Long transStatus) 
    {
        this.transStatus = transStatus;
    }

    public Long getTransStatus() 
    {
        return transStatus;
    }
    public void setIdStepRead(Long idStepRead) 
    {
        this.idStepRead = idStepRead;
    }

    public Long getIdStepRead() 
    {
        return idStepRead;
    }
    public void setIdStepWrite(Long idStepWrite) 
    {
        this.idStepWrite = idStepWrite;
    }

    public Long getIdStepWrite() 
    {
        return idStepWrite;
    }
    public void setIdStepInput(Long idStepInput) 
    {
        this.idStepInput = idStepInput;
    }

    public Long getIdStepInput() 
    {
        return idStepInput;
    }
    public void setIdStepOutput(Long idStepOutput) 
    {
        this.idStepOutput = idStepOutput;
    }

    public Long getIdStepOutput() 
    {
        return idStepOutput;
    }
    public void setIdStepUpdate(Long idStepUpdate) 
    {
        this.idStepUpdate = idStepUpdate;
    }

    public Long getIdStepUpdate() 
    {
        return idStepUpdate;
    }
    public void setIdDatabaseLog(Long idDatabaseLog) 
    {
        this.idDatabaseLog = idDatabaseLog;
    }

    public Long getIdDatabaseLog() 
    {
        return idDatabaseLog;
    }
    public void setTableNameLog(String tableNameLog) 
    {
        this.tableNameLog = tableNameLog;
    }

    public String getTableNameLog() 
    {
        return tableNameLog;
    }
    public void setUseBatchid(Integer useBatchid) 
    {
        this.useBatchid = useBatchid;
    }

    public Integer getUseBatchid() 
    {
        return useBatchid;
    }
    public void setUseLogfield(Integer useLogfield) 
    {
        this.useLogfield = useLogfield;
    }

    public Integer getUseLogfield() 
    {
        return useLogfield;
    }
    public void setIdDatabaseMaxdate(Long idDatabaseMaxdate) 
    {
        this.idDatabaseMaxdate = idDatabaseMaxdate;
    }

    public Long getIdDatabaseMaxdate() 
    {
        return idDatabaseMaxdate;
    }
    public void setTableNameMaxdate(String tableNameMaxdate) 
    {
        this.tableNameMaxdate = tableNameMaxdate;
    }

    public String getTableNameMaxdate() 
    {
        return tableNameMaxdate;
    }
    public void setFieldNameMaxdate(String fieldNameMaxdate) 
    {
        this.fieldNameMaxdate = fieldNameMaxdate;
    }

    public String getFieldNameMaxdate() 
    {
        return fieldNameMaxdate;
    }
    public void setOffsetMaxdate(Long offsetMaxdate) 
    {
        this.offsetMaxdate = offsetMaxdate;
    }

    public Long getOffsetMaxdate() 
    {
        return offsetMaxdate;
    }
    public void setDiffMaxdate(Long diffMaxdate) 
    {
        this.diffMaxdate = diffMaxdate;
    }

    public Long getDiffMaxdate() 
    {
        return diffMaxdate;
    }
    public void setCreatedUser(String createdUser) 
    {
        this.createdUser = createdUser;
    }

    public String getCreatedUser() 
    {
        return createdUser;
    }
    public void setCreatedDate(Date createdDate) 
    {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() 
    {
        return createdDate;
    }
    public void setModifiedUser(String modifiedUser) 
    {
        this.modifiedUser = modifiedUser;
    }

    public String getModifiedUser() 
    {
        return modifiedUser;
    }
    public void setModifiedDate(Date modifiedDate) 
    {
        this.modifiedDate = modifiedDate;
    }

    public Date getModifiedDate() 
    {
        return modifiedDate;
    }
    public void setSizeRowset(Long sizeRowset) 
    {
        this.sizeRowset = sizeRowset;
    }

    public Long getSizeRowset() 
    {
        return sizeRowset;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }


    public String getParGroupName() {
        return parGroupName;
    }

    public void setParGroupName(String parGroupName) {
        this.parGroupName = parGroupName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTransformation", getId())
            .append("idDirectory", getIdDirectory())
            .append("name", getName())
            .append("description", getDescription())
            .append("extendedDescription", getExtendedDescription())
            .append("transVersion", getTransVersion())
            .append("transStatus", getTransStatus())
            .append("idStepRead", getIdStepRead())
            .append("idStepWrite", getIdStepWrite())
            .append("idStepInput", getIdStepInput())
            .append("idStepOutput", getIdStepOutput())
            .append("idStepUpdate", getIdStepUpdate())
            .append("idDatabaseLog", getIdDatabaseLog())
            .append("tableNameLog", getTableNameLog())
            .append("useBatchid", getUseBatchid())
            .append("useLogfield", getUseLogfield())
            .append("idDatabaseMaxdate", getIdDatabaseMaxdate())
            .append("tableNameMaxdate", getTableNameMaxdate())
            .append("fieldNameMaxdate", getFieldNameMaxdate())
            .append("offsetMaxdate", getOffsetMaxdate())
            .append("diffMaxdate", getDiffMaxdate())
            .append("createdUser", getCreatedUser())
            .append("createdDate", getCreatedDate())
            .append("modifiedUser", getModifiedUser())
            .append("modifiedDate", getModifiedDate())
            .append("sizeRowset", getSizeRowset())
            .toString();
    }
}
