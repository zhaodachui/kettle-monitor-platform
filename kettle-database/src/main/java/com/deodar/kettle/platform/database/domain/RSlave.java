package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 r_slave
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RSlave extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String hostName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String port;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String webAppName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String username;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String password;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proxyHostName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proxyPort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String nonProxyHosts;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer master;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    private double loadAvg;     //负载指数

    private String runStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setHostName(String hostName) 
    {
        this.hostName = hostName;
    }

    public String getHostName() 
    {
        return hostName;
    }
    public void setPort(String port) 
    {
        this.port = port;
    }

    public String getPort() 
    {
        return port;
    }
    public void setWebAppName(String webAppName) 
    {
        this.webAppName = webAppName;
    }

    public String getWebAppName() 
    {
        return webAppName;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setProxyHostName(String proxyHostName) 
    {
        this.proxyHostName = proxyHostName;
    }

    public String getProxyHostName() 
    {
        return proxyHostName;
    }
    public void setProxyPort(String proxyPort) 
    {
        this.proxyPort = proxyPort;
    }

    public String getProxyPort() 
    {
        return proxyPort;
    }
    public void setNonProxyHosts(String nonProxyHosts) 
    {
        this.nonProxyHosts = nonProxyHosts;
    }

    public String getNonProxyHosts() 
    {
        return nonProxyHosts;
    }
    public void setMaster(Integer master) 
    {
        this.master = master;
    }

    public Integer getMaster() 
    {
        return master;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public double getLoadAvg() {
        return loadAvg;
    }

    public void setLoadAvg(double loadAvg) {
        this.loadAvg = loadAvg;
    }

    public String getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(String runStatus) {
        this.runStatus = runStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("hostName", getHostName())
            .append("port", getPort())
            .append("webAppName", getWebAppName())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("proxyHostName", getProxyHostName())
            .append("proxyPort", getProxyPort())
            .append("nonProxyHosts", getNonProxyHosts())
            .append("master", getMaster())
            .append("status", getStatus())
            .toString();
    }
}
