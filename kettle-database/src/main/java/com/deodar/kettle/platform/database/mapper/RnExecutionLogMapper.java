package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RnExecutionLog;

import java.util.List;
import java.util.Map;


/**
 * 任务执行日志Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-26
 */
public interface RnExecutionLogMapper {
    /**
     * 查询任务执行日志
     * 
     * @param id 任务执行日志ID
     * @return 任务执行日志
     */
    public RnExecutionLog selectRnExecutionLogById(String id);

    /**
     * 查询任务执行日志列表
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 任务执行日志集合
     */
    public List<RnExecutionLog> selectRnExecutionLogList(RnExecutionLog rnExecutionLog);

    /**
     * 新增任务执行日志
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 结果
     */
    public int insertRnExecutionLog(RnExecutionLog rnExecutionLog);

    /**
     * 修改任务执行日志
     * 
     * @param rnExecutionLog 任务执行日志
     * @return 结果
     */
    public int updateRnExecutionLog(RnExecutionLog rnExecutionLog);

    /**
     * 删除任务执行日志
     * 
     * @param id 任务执行日志ID
     * @return 结果
     */
    public int deleteRnExecutionLogById(String id);

    /**
     * 批量删除任务执行日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRnExecutionLogByIds(String[] ids);

    List<Map<String,Object>> selectTaskGroup(RnExecutionLog rnExecutionLog);

}
