package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RnStepStatus;

import java.util.List;

/**
 * 任务中间过程Mapper接口
 * 
 * @author
 * @date 2020-04-08
 */
public interface RnStepStatusMapper {
    /**
     * 查询任务中间过程
     * 
     * @param id 任务中间过程ID
     * @return 任务中间过程
     */
    public RnStepStatus selectRnStepStatusById(String id);

    /**
     * 查询任务中间过程列表
     * 
     * @param RnStepStatus 任务中间过程
     * @return 任务中间过程集合
     */
    public List<RnStepStatus> selectRnStepStatusList(RnStepStatus RnStepStatus);

    /**
     * 新增任务中间过程
     * 
     * @param RnStepStatus 任务中间过程
     * @return 结果
     */
    public int insertRnStepStatus(RnStepStatus RnStepStatus);

    /**
     * 修改任务中间过程
     * 
     * @param RnStepStatus 任务中间过程
     * @return 结果
     */
    public int updateRnStepStatus(RnStepStatus RnStepStatus);

    /**
     * 删除任务中间过程
     * 
     * @param id 任务中间过程ID
     * @return 结果
     */
    public int deleteRnStepStatusById(String id);

    /**
     * 批量删除任务中间过程
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRnStepStatusByIds(String[] ids);

    /**
     * 根据任务id批量删除
     * @param taskId 任务id
     * @return
     */
    int deleteStepStatusByTaskId(String taskId);
}
