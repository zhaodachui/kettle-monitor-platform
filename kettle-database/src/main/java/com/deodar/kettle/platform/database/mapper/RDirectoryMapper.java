package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RDirectory;
import java.util.List;


/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface RDirectoryMapper {
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDirectory 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RDirectory selectRDirectoryById(Integer idDirectory);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rDirectory 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RDirectory> selectRDirectoryList(RDirectory rDirectory);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rDirectory 【请填写功能名称】
     * @return 结果
     */
    public int insertRDirectory(RDirectory rDirectory);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rDirectory 【请填写功能名称】
     * @return 结果
     */
    public int updateRDirectory(RDirectory rDirectory);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idDirectory 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRDirectoryById(Integer idDirectory);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDirectorys 需要删除的数据ID
     * @return 结果
     */
    public int deleteRDirectoryByIds(String[] idDirectorys);

    Long getMaxId();
}
