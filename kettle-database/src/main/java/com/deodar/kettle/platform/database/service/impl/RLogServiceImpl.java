package com.deodar.kettle.platform.database.service.impl;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RLog;
import com.deodar.kettle.platform.database.mapper.RLogMapper;
import com.deodar.kettle.platform.database.service.IRLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service
public class RLogServiceImpl implements IRLogService {
    @Autowired
    private RLogMapper rLogMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idLog 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public RLog selectRLogById(Long idLog)
    {
        return rLogMapper.selectRLogById(idLog);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rLog 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RLog> selectRLogList(RLog rLog)
    {
        return rLogMapper.selectRLogList(rLog);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param rLog 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRLog(RLog rLog)
    {
        return rLogMapper.insertRLog(rLog);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param rLog 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRLog(RLog rLog)
    {
        return rLogMapper.updateRLog(rLog);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRLogByIds(String ids)
    {
        return rLogMapper.deleteRLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idLog 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteRLogById(Long idLog)
    {
        return rLogMapper.deleteRLogById(idLog);
    }
}
