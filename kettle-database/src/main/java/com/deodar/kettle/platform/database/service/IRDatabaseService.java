package com.deodar.kettle.platform.database.service;

import com.deodar.kettle.platform.database.domain.RDatabase;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface IRDatabaseService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDatabase 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RDatabase selectRDatabaseById(Long idDatabase);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rDatabase 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RDatabase> selectRDatabaseList(RDatabase rDatabase);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rDatabase 【请填写功能名称】
     * @return 结果
     */
    public int insertRDatabase(RDatabase rDatabase);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rDatabase 【请填写功能名称】
     * @return 结果
     */
    public int updateRDatabase(RDatabase rDatabase);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRDatabaseByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDatabase 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRDatabaseById(Long idDatabase);
}
