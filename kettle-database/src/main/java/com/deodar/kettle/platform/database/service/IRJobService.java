package com.deodar.kettle.platform.database.service;

import com.deodar.kettle.platform.database.domain.RJob;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface IRJobService {
    /**
     * 查询【请填写功能名称】
     * 
     * @param idJob 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RJob selectRJobById(Integer idJob);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rJob 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RJob> selectRJobList(RJob rJob);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rJob 【请填写功能名称】
     * @return 结果
     */
    public int insertRJob(RJob rJob);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rJob 【请填写功能名称】
     * @return 结果
     */
    public int updateRJob(RJob rJob);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRJobByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJob 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRJobById(Integer idJob);
}
