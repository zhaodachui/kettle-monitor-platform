package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 【请填写功能名称】对象 r_job
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RJob extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer idDirectory;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String description;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extendedDescription;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String jobVersion;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long jobStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDatabaseLog;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tableNameLog;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String createdUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String modifiedUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date modifiedDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer useBatchId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer passBatchId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer useLogfield;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sharedFile;

    private String parGroupId;  //参数组id


    private String parGroupName;

    private Long deptId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDirectory() {
        return idDirectory;
    }

    public void setIdDirectory(Integer idDirectory) {
        this.idDirectory = idDirectory;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setExtendedDescription(String extendedDescription) 
    {
        this.extendedDescription = extendedDescription;
    }

    public String getExtendedDescription() 
    {
        return extendedDescription;
    }
    public void setJobVersion(String jobVersion) 
    {
        this.jobVersion = jobVersion;
    }

    public String getJobVersion() 
    {
        return jobVersion;
    }
    public void setJobStatus(Long jobStatus) 
    {
        this.jobStatus = jobStatus;
    }

    public Long getJobStatus() 
    {
        return jobStatus;
    }
    public void setIdDatabaseLog(Long idDatabaseLog) 
    {
        this.idDatabaseLog = idDatabaseLog;
    }

    public Long getIdDatabaseLog() 
    {
        return idDatabaseLog;
    }
    public void setTableNameLog(String tableNameLog) 
    {
        this.tableNameLog = tableNameLog;
    }

    public String getTableNameLog() 
    {
        return tableNameLog;
    }
    public void setCreatedUser(String createdUser) 
    {
        this.createdUser = createdUser;
    }

    public String getCreatedUser() 
    {
        return createdUser;
    }
    public void setCreatedDate(Date createdDate) 
    {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() 
    {
        return createdDate;
    }
    public void setModifiedUser(String modifiedUser) 
    {
        this.modifiedUser = modifiedUser;
    }

    public String getModifiedUser() 
    {
        return modifiedUser;
    }
    public void setModifiedDate(Date modifiedDate) 
    {
        this.modifiedDate = modifiedDate;
    }

    public Date getModifiedDate() 
    {
        return modifiedDate;
    }
    public void setUseBatchId(Integer useBatchId) 
    {
        this.useBatchId = useBatchId;
    }

    public Integer getUseBatchId() 
    {
        return useBatchId;
    }
    public void setPassBatchId(Integer passBatchId) 
    {
        this.passBatchId = passBatchId;
    }

    public Integer getPassBatchId() 
    {
        return passBatchId;
    }
    public void setUseLogfield(Integer useLogfield) 
    {
        this.useLogfield = useLogfield;
    }

    public Integer getUseLogfield() 
    {
        return useLogfield;
    }
    public void setSharedFile(String sharedFile) 
    {
        this.sharedFile = sharedFile;
    }

    public String getSharedFile() 
    {
        return sharedFile;
    }

    public String getParGroupId() {
        return parGroupId;
    }

    public void setParGroupId(String parGroupId) {
        this.parGroupId = parGroupId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getParGroupName() {
        return parGroupName;
    }

    public void setParGroupName(String parGroupName) {
        this.parGroupName = parGroupName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idJob", getId())
            .append("idDirectory", getIdDirectory())
            .append("name", getName())
            .append("description", getDescription())
            .append("extendedDescription", getExtendedDescription())
            .append("jobVersion", getJobVersion())
            .append("jobStatus", getJobStatus())
            .append("idDatabaseLog", getIdDatabaseLog())
            .append("tableNameLog", getTableNameLog())
            .append("createdUser", getCreatedUser())
            .append("createdDate", getCreatedDate())
            .append("modifiedUser", getModifiedUser())
            .append("modifiedDate", getModifiedDate())
            .append("useBatchId", getUseBatchId())
            .append("passBatchId", getPassBatchId())
            .append("useLogfield", getUseLogfield())
            .append("sharedFile", getSharedFile())
            .toString();
    }
}
