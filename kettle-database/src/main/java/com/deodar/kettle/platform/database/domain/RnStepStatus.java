package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;

/**
 * 步骤明细对象 rn_stepstatus
 * 
 * @author ruoyi
 * @date 2020-04-28
 */
public class RnStepStatus extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** null */
    @Excel(name = "null")
    private String id;

    /** 排序 */
    @Excel(name = "排序")
    private Integer taskSort;

    /** 步骤名称 */
    @Excel(name = "步骤名称")
    private String stepName;

    /** CopyNr */
    @Excel(name = "CopyNr")
    private int copy;

    /** 读取 */
    @Excel(name = "读取")
    private Long linesRead;

    /** 写入 */
    @Excel(name = "写入")
    private Long linesWritten;

    /** 输入 */
    @Excel(name = "输入")
    private Long linesInput;

    /** 输出 */
    @Excel(name = "输出")
    private Long linesOutput;

    /** 更新 */
    @Excel(name = "更新")
    private Long linesUpdated;

    /** 拒绝 */
    @Excel(name = "拒绝")
    private Long linesRejected;

    /** null */
    @Excel(name = "null")
    private Long logErrors;

    /** null */
    @Excel(name = "null")
    private String statusDescription;

    /** null */
    @Excel(name = "null")
    private Double logSeconds;

    /** 速度 */
    @Excel(name = "速度")
    private String speed;

    /** pr/in/out */
    @Excel(name = "pr/in/out")
    private String logPriority;

    /** 停止 */
    @Excel(name = "停止")
    private String stopped;

    /** 暂停 */
    @Excel(name = "暂停")
    private String paused;

    /** 任务id */
    @Excel(name = "任务id")
    private String taskId;


    public RnStepStatus() {
    }

    public RnStepStatus(String taskId) {
        this.taskId = taskId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTaskSort() {
        return taskSort;
    }

    public void setTaskSort(Integer taskSort) {
        this.taskSort = taskSort;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public int getCopy() {
        return copy;
    }

    public void setCopy(int copy) {
        this.copy = copy;
    }

    public Long getLinesRead() {
        return linesRead;
    }

    public void setLinesRead(Long linesRead) {
        this.linesRead = linesRead;
    }

    public Long getLinesWritten() {
        return linesWritten;
    }

    public void setLinesWritten(Long linesWritten) {
        this.linesWritten = linesWritten;
    }

    public Long getLinesInput() {
        return linesInput;
    }

    public void setLinesInput(Long linesInput) {
        this.linesInput = linesInput;
    }

    public Long getLinesOutput() {
        return linesOutput;
    }

    public void setLinesOutput(Long linesOutput) {
        this.linesOutput = linesOutput;
    }

    public Long getLinesUpdated() {
        return linesUpdated;
    }

    public void setLinesUpdated(Long linesUpdated) {
        this.linesUpdated = linesUpdated;
    }

    public Long getLinesRejected() {
        return linesRejected;
    }

    public void setLinesRejected(Long linesRejected) {
        this.linesRejected = linesRejected;
    }

    public Long getLogErrors() {
        return logErrors;
    }

    public void setLogErrors(Long logErrors) {
        this.logErrors = logErrors;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public Double getLogSeconds() {
        return logSeconds;
    }

    public void setLogSeconds(Double logSeconds) {
        this.logSeconds = logSeconds;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLogPriority() {
        return logPriority;
    }

    public void setLogPriority(String logPriority) {
        this.logPriority = logPriority;
    }

    public String getStopped() {
        return stopped;
    }

    public void setStopped(String stopped) {
        this.stopped = stopped;
    }

    public String getPaused() {
        return paused;
    }

    public void setPaused(String paused) {
        this.paused = paused;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskSort", getTaskSort())
            .append("stepName", getStepName())
            .append("copy", getCopy())
            .append("linesRead", getLinesRead())
            .append("linesWritten", getLinesWritten())
            .append("linesInput", getLinesInput())
            .append("linesOutput", getLinesOutput())
            .append("linesUpdated", getLinesUpdated())
            .append("linesRejected", getLinesRejected())
            .append("logErrors", getLogErrors())
            .append("statusDescription", getStatusDescription())
            .append("logSeconds", getLogSeconds())
            .append("speed", getSpeed())
            .append("logPriority", getLogPriority())
            .append("stopped", getStopped())
            .append("paused", getPaused())
            .append("taskId", getTaskId())
            .toString();
    }
}
