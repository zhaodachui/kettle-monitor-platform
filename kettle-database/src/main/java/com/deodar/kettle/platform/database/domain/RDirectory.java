package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 r_directory
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RDirectory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idDirectory;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDirectoryParent;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String directoryName;

    private Integer type;

    private String parentName;

    public void setIdDirectory(Long idDirectory)
    {
        this.idDirectory = idDirectory;
    }

    public Long getIdDirectory()
    {
        return idDirectory;
    }
    public void setIdDirectoryParent(Long idDirectoryParent)
    {
        this.idDirectoryParent = idDirectoryParent;
    }

    public Long getIdDirectoryParent()
    {
        return idDirectoryParent;
    }
    public void setDirectoryName(String directoryName) 
    {
        this.directoryName = directoryName;
    }

    public String getDirectoryName() 
    {
        return directoryName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDirectory", getIdDirectory())
            .append("idDirectoryParent", getIdDirectoryParent())
            .append("directoryName", getDirectoryName())
            .append("parentName",getParentName())
                .append("type",getType())
            .toString();
    }
}
