package com.deodar.kettle.platform.database.service.impl;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RJob;
import com.deodar.kettle.platform.database.domain.RnTaskParameterGroup;
import com.deodar.common.utils.StringUtils;
import com.deodar.kettle.platform.database.mapper.RJobMapper;
import com.deodar.kettle.platform.database.mapper.RnTaskParameterGroupMapper;
import com.deodar.kettle.platform.database.service.IRJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service("rJobService")
public class RJobServiceImpl implements IRJobService {
    @Autowired
    private RJobMapper rJobMapper;

    @Autowired
    private RnTaskParameterGroupMapper rnTaskParameterGroupMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idJob 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public RJob selectRJobById(Integer idJob)
    {
        return rJobMapper.selectRJobById(idJob);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rJob 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RJob> selectRJobList(RJob rJob)
    {
        return rJobMapper.selectRJobList(rJob);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param rJob 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRJob(RJob rJob)
    {
        return rJobMapper.insertRJob(rJob);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param rJob 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRJob(RJob rJob) {
        if(StringUtils.isNotEmpty(rJob.getParGroupId())){
            rnTaskParameterGroupMapper.deleteByRId(rJob.getId());
            rnTaskParameterGroupMapper.insertRnTaskParameterGroup(new RnTaskParameterGroup(rJob.getId(),rJob.getParGroupId()));
        }
        return rJobMapper.updateRJob(rJob);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRJobByIds(String ids)
    {
        return rJobMapper.deleteRJobByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJob 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteRJobById(Integer idJob) {
        return rJobMapper.deleteRJobById(idJob);
    }
}
