package com.deodar.kettle.platform.database.service.impl;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RDatabase;
import com.deodar.kettle.platform.database.mapper.RDatabaseMapper;
import com.deodar.kettle.platform.database.service.IRDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service("rDatabaseService")
public class RDatabaseServiceImpl implements IRDatabaseService {
    @Autowired
    private RDatabaseMapper rDatabaseMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idDatabase 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public RDatabase selectRDatabaseById(Long idDatabase)
    {
        return rDatabaseMapper.selectRDatabaseById(idDatabase);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rDatabase 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RDatabase> selectRDatabaseList(RDatabase rDatabase)
    {
        return rDatabaseMapper.selectRDatabaseList(rDatabase);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param rDatabase 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRDatabase(RDatabase rDatabase)
    {
        return rDatabaseMapper.insertRDatabase(rDatabase);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param rDatabase 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRDatabase(RDatabase rDatabase)
    {
        return rDatabaseMapper.updateRDatabase(rDatabase);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRDatabaseByIds(String ids)
    {
        return rDatabaseMapper.deleteRDatabaseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDatabase 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteRDatabaseById(Long idDatabase)
    {
        return rDatabaseMapper.deleteRDatabaseById(idDatabase);
    }
}
