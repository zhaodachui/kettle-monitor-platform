package com.deodar.kettle.platform.database.service.impl;

import java.util.List;

import com.deodar.kettle.platform.database.domain.RTransformation;
import com.deodar.kettle.platform.database.domain.RnTaskParameterGroup;
import com.deodar.common.utils.StringUtils;
import com.deodar.kettle.platform.database.mapper.RTransformationMapper;
import com.deodar.kettle.platform.database.mapper.RnTaskParameterGroupMapper;
import com.deodar.kettle.platform.database.service.IRTransformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service("transformationService")
public class RTransformationServiceImpl implements IRTransformationService {
    @Autowired
    private RTransformationMapper rTransformationMapper;

    @Autowired
    private RnTaskParameterGroupMapper rnTaskParameterGroupMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idTransformation 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public RTransformation selectRTransformationById(Integer idTransformation) {
        return rTransformationMapper.selectRTransformationById(idTransformation);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RTransformation> selectRTransformationList(RTransformation rTransformation)
    {
        return rTransformationMapper.selectRTransformationList(rTransformation);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRTransformation(RTransformation rTransformation)
    {
        return rTransformationMapper.insertRTransformation(rTransformation);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRTransformation(RTransformation rTransformation) {
        if(StringUtils.isNotEmpty(rTransformation.getParGroupId())){
            rnTaskParameterGroupMapper.deleteByRId(rTransformation.getId());
            rnTaskParameterGroupMapper.insertRnTaskParameterGroup(new RnTaskParameterGroup(rTransformation.getId(),
                       rTransformation.getParGroupId()));
        }
        return rTransformationMapper.updateRTransformation(rTransformation);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRTransformationByIds(String ids)
    {
        return rTransformationMapper.deleteRTransformationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idTransformation 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteRTransformationById(Integer idTransformation)
    {
        return rTransformationMapper.deleteRTransformationById(idTransformation);
    }
}
