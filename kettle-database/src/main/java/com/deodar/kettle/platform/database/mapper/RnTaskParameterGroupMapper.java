package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RnTaskParameterGroup;

import java.util.List;

/**
 * 任务与参数关系Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-28
 */
public interface RnTaskParameterGroupMapper {
    /**
     * 查询任务与参数关系
     * 
     * @param id 任务与参数关系ID
     * @return 任务与参数关系
     */
    public RnTaskParameterGroup selectRnTaskParameterGroupById(String id);

    /**
     * 查询任务与参数关系列表
     * 
     * @param rnTaskParameterGroup 任务与参数关系
     * @return 任务与参数关系集合
     */
    public List<RnTaskParameterGroup> selectRnTaskParameterGroupList(RnTaskParameterGroup rnTaskParameterGroup);

    /**
     * 新增任务与参数关系
     * 
     * @param rnTaskParameterGroup 任务与参数关系
     * @return 结果
     */
    public int insertRnTaskParameterGroup(RnTaskParameterGroup rnTaskParameterGroup);

    /**
     * 修改任务与参数关系
     * 
     * @param rnTaskParameterGroup 任务与参数关系
     * @return 结果
     */
    public int updateRnTaskParameterGroup(RnTaskParameterGroup rnTaskParameterGroup);

    /**
     * 删除任务与参数关系
     * 
     * @param id 任务与参数关系ID
     * @return 结果
     */
    public int deleteRnTaskParameterGroupById(String id);

    /**
     * 批量删除任务与参数关系
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRnTaskParameterGroupByIds(String[] ids);

    int deleteByRId(Integer id);
}
