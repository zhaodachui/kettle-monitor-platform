package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 r_log
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idLog;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idLoglevel;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String logtype;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String filename;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fileextention;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer addDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer addTime;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDatabaseLog;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tableNameLog;

    public void setIdLog(Long idLog) 
    {
        this.idLog = idLog;
    }

    public Long getIdLog() 
    {
        return idLog;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIdLoglevel(Long idLoglevel) 
    {
        this.idLoglevel = idLoglevel;
    }

    public Long getIdLoglevel() 
    {
        return idLoglevel;
    }
    public void setLogtype(String logtype) 
    {
        this.logtype = logtype;
    }

    public String getLogtype() 
    {
        return logtype;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setFileextention(String fileextention) 
    {
        this.fileextention = fileextention;
    }

    public String getFileextention() 
    {
        return fileextention;
    }
    public void setAddDate(Integer addDate) 
    {
        this.addDate = addDate;
    }

    public Integer getAddDate() 
    {
        return addDate;
    }
    public void setAddTime(Integer addTime) 
    {
        this.addTime = addTime;
    }

    public Integer getAddTime() 
    {
        return addTime;
    }
    public void setIdDatabaseLog(Long idDatabaseLog) 
    {
        this.idDatabaseLog = idDatabaseLog;
    }

    public Long getIdDatabaseLog() 
    {
        return idDatabaseLog;
    }
    public void setTableNameLog(String tableNameLog) 
    {
        this.tableNameLog = tableNameLog;
    }

    public String getTableNameLog() 
    {
        return tableNameLog;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idLog", getIdLog())
            .append("name", getName())
            .append("idLoglevel", getIdLoglevel())
            .append("logtype", getLogtype())
            .append("filename", getFilename())
            .append("fileextention", getFileextention())
            .append("addDate", getAddDate())
            .append("addTime", getAddTime())
            .append("idDatabaseLog", getIdDatabaseLog())
            .append("tableNameLog", getTableNameLog())
            .toString();
    }
}
