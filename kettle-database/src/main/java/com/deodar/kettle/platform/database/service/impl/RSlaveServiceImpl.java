package com.deodar.kettle.platform.database.service.impl;

import java.util.List;

import com.deodar.kettle.platform.common.util.KettleEncr;
import com.deodar.kettle.platform.database.domain.RSlave;
import com.deodar.kettle.platform.database.mapper.RSlaveMapper;
import com.deodar.kettle.platform.database.service.IRSlaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service
public class RSlaveServiceImpl implements IRSlaveService {
    @Autowired
    private RSlaveMapper rSlaveMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idSlave 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public RSlave selectRSlaveById(Integer idSlave)
    {
        return rSlaveMapper.selectRSlaveById(idSlave);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rSlave 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RSlave> selectRSlaveList(RSlave rSlave)
    {
        return rSlaveMapper.selectRSlaveList(rSlave);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param rSlave 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRSlave(RSlave rSlave) {
        rSlave.setPassword(KettleEncr.encryptPassword(rSlave.getPassword()));
        return rSlaveMapper.insertRSlave(rSlave);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param rSlave 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRSlave(RSlave rSlave) {
        rSlave.setPassword(KettleEncr.encryptPassword(rSlave.getPassword()));
        return rSlaveMapper.updateRSlave(rSlave);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRSlaveByIds(String ids)
    {
        return rSlaveMapper.deleteRSlaveByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idSlave 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteRSlaveById(Integer idSlave)
    {
        return rSlaveMapper.deleteRSlaveById(idSlave);
    }

    @Override
    public List<RSlave> getAllSlaveInfo() {
        return rSlaveMapper.selectRSlaveList(new RSlave());
    }


}
