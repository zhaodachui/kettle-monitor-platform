package com.deodar.kettle.platform.database.mapper;

import com.deodar.kettle.platform.database.domain.RTransformation;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public interface RTransformationMapper {
    /**
     * 查询【请填写功能名称】
     * 
     * @param idTransformation 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public RTransformation selectRTransformationById(Integer idTransformation);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RTransformation> selectRTransformationList(RTransformation rTransformation);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 结果
     */
    public int insertRTransformation(RTransformation rTransformation);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rTransformation 【请填写功能名称】
     * @return 结果
     */
    public int updateRTransformation(RTransformation rTransformation);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idTransformation 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteRTransformationById(Integer idTransformation);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idTransformations 需要删除的数据ID
     * @return 结果
     */
    public int deleteRTransformationByIds(String[] idTransformations);
}
