package com.deodar.kettle.platform.database.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.deodar.common.core.domain.Ztree;
import com.deodar.kettle.platform.database.domain.RDirectory;
import com.deodar.kettle.platform.database.mapper.RDirectoryMapper;
import com.deodar.kettle.platform.database.service.IRDirectoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
@Service("directoryService")
public class RDirectoryServiceImpl implements IRDirectoryService {
    @Autowired
    private RDirectoryMapper rDirectoryMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idDirectory 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public RDirectory selectRDirectoryById(Integer idDirectory)
    {
        return rDirectoryMapper.selectRDirectoryById(idDirectory);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rDirectory 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RDirectory> selectRDirectoryList(RDirectory rDirectory)
    {
        return rDirectoryMapper.selectRDirectoryList(rDirectory);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param rDirectory 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRDirectory(RDirectory rDirectory) {
        Long id = rDirectoryMapper.getMaxId() +1L;
        rDirectory.setIdDirectory(id);
        return rDirectoryMapper.insertRDirectory(rDirectory);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param rDirectory 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRDirectory(RDirectory rDirectory)
    {
        return rDirectoryMapper.updateRDirectory(rDirectory);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRDirectoryByIds(String ids)
    {
        return rDirectoryMapper.deleteRDirectoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDirectory 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteRDirectoryById(Integer idDirectory)
    {
        return rDirectoryMapper.deleteRDirectoryById(idDirectory);
    }


    /**
     * 查询目录分类树列表
     *
     * @return 所有目录分类信息
     */
    @Override
    public List<Ztree> selectRDirectoryTree(RDirectory directory) {
        List<RDirectory> rDirectoryList = rDirectoryMapper.selectRDirectoryList(directory);
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (RDirectory rDirectory : rDirectoryList){
            Ztree ztree = new Ztree();
            ztree.setId(rDirectory.getIdDirectory());
            ztree.setpId(rDirectory.getIdDirectoryParent());
            ztree.setName(rDirectory.getDirectoryName());
            ztree.setTitle(rDirectory.getDirectoryName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
