package com.deodar.kettle.platform.database.domain;

import com.deodar.common.utils.IDUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 任务与参数关系对象 rn_task_parameter_group
 * 
 * @author ruoyi
 * @date 2020-04-28
 */
public class RnTaskParameterGroup extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** null */
    private String id;

    /** trans的id或者job的id */
    @Excel(name = "trans的id或者job的id")
    private Integer rId;

    /** 参数组id */
    @Excel(name = "参数组id")
    private String paramGroupId;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }
    public void setrId(Integer rId){
        this.rId = rId;
    }

    public Integer getrId(){
        return rId;
    }
    public void setParamGroupId(String paramGroupId){
        this.paramGroupId = paramGroupId;
    }

    public String getParamGroupId(){
        return paramGroupId;
    }

    public RnTaskParameterGroup(Integer rId, String paramGroupId) {
        this.id = IDUtil.getSnowflakeId();
        this.rId = rId;
        this.paramGroupId = paramGroupId;
        setUpdateTime(new Date());

    }

    public RnTaskParameterGroup() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("rId", getrId())
            .append("updateTime", getUpdateTime())
            .append("paramGroupId", getParamGroupId())
            .toString();
    }
}
