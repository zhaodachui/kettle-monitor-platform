package com.deodar.kettle.platform.database.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 r_database
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RDatabase extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDatabaseType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDatabaseContype;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String hostName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String databaseName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long port;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String username;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String password;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String servername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dataTbs;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String indexTbs;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIdDatabaseType(Long idDatabaseType) 
    {
        this.idDatabaseType = idDatabaseType;
    }

    public Long getIdDatabaseType() 
    {
        return idDatabaseType;
    }
    public void setIdDatabaseContype(Long idDatabaseContype) 
    {
        this.idDatabaseContype = idDatabaseContype;
    }

    public Long getIdDatabaseContype() 
    {
        return idDatabaseContype;
    }
    public void setHostName(String hostName) 
    {
        this.hostName = hostName;
    }

    public String getHostName() 
    {
        return hostName;
    }
    public void setDatabaseName(String databaseName) 
    {
        this.databaseName = databaseName;
    }

    public String getDatabaseName() 
    {
        return databaseName;
    }
    public void setPort(Long port) 
    {
        this.port = port;
    }

    public Long getPort() 
    {
        return port;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setServername(String servername) 
    {
        this.servername = servername;
    }

    public String getServername() 
    {
        return servername;
    }
    public void setDataTbs(String dataTbs) 
    {
        this.dataTbs = dataTbs;
    }

    public String getDataTbs() 
    {
        return dataTbs;
    }
    public void setIndexTbs(String indexTbs) 
    {
        this.indexTbs = indexTbs;
    }

    public String getIndexTbs() 
    {
        return indexTbs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDatabase", getId())
            .append("name", getName())
            .append("idDatabaseType", getIdDatabaseType())
            .append("idDatabaseContype", getIdDatabaseContype())
            .append("hostName", getHostName())
            .append("databaseName", getDatabaseName())
            .append("port", getPort())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("servername", getServername())
            .append("dataTbs", getDataTbs())
            .append("indexTbs", getIndexTbs())
            .toString();
    }
}
