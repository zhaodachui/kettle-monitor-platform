package com.deodar.kettle.platform.parameter.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.deodar.kettle.platform.parameter.domain.RnParameter;
import com.deodar.kettle.platform.parameter.mapper.RnParameterMapper;
import com.deodar.kettle.platform.parameter.service.IRnParameterService;
import com.deodar.common.utils.IDUtil;
import com.deodar.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 参数组Service业务层处理
 * 
 * @author Administrator
 * @date 2020-04-08
 */
@Service
public class RnParameterServiceImpl implements IRnParameterService {
    @Autowired
    private RnParameterMapper rnParameterrMapper;

    /**
     * 查询参数组
     * 
     * @param id 参数组ID
     * @return 参数组
     */
    @Override
    public RnParameter selectRnParameterById(String id)
    {
        return rnParameterrMapper.selectRnParameterById(id);
    }

    /**
     * 查询参数组列表
     * 
     * @param rnParameterr 参数组
     * @return 参数组
     */
    @Override
    public List<RnParameter> selectRnParameterList(RnParameter rnParameterr) {
        return rnParameterrMapper.selectRnParameterList(rnParameterr);
    }

    /**
     * 新增参数组
     * 
     * @param rnParameterr 参数组
     * @return 结果
     */
    @Override
    public int insertRnParameter(RnParameter rnParameterr) {
        rnParameterr.setId(IDUtil.getSnowflakeId());
        rnParameterr.setUpdateTime(new Date());
        rnParameterr.setName(buildParam(rnParameterr.getName()));
        return rnParameterrMapper.insertRnParameter(rnParameterr);
    }

    /**
     * 修改参数组
     * 
     * @param rnParameterr 参数组
     * @return 结果
     */
    @Override
    public int updateRnParameter(RnParameter rnParameterr) {
        rnParameterr.setUpdateTime(new Date());
        rnParameterr.setName(buildParam(rnParameterr.getName()));
        return rnParameterrMapper.updateRnParameter(rnParameterr);
    }

    /**
     * 删除参数组对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRnParameterByIds(String ids)
    {
        return rnParameterrMapper.deleteRnParameterByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除参数组信息
     * 
     * @param id 参数组ID
     * @return 结果
     */
    @Override
    public int deleteRnParameterById(String id)
    {
        return rnParameterrMapper.deleteRnParameterById(id);
    }

    @Override
    public List<RnParameter> selectListByGroupId(String groupId) {
        return rnParameterrMapper.selectListByGroupId(groupId);
    }

    @Override
    public Map<String,String> selectMapByGroupId(String groupId){
        if(groupId == null || "".equals(groupId)){
            return null;
        }
        List<RnParameter> parameterList = rnParameterrMapper.selectListByGroupId(groupId);
        if(parameterList !=null && parameterList.size() >0){
            Map<String,String> map = new HashMap<>();
            for(RnParameter parameter : parameterList){
                String name = parameter.getName().replaceAll("[$]","");
                map.put(name,parameter.getValue());
            }
            return map;
        }
        return null;
    }

    private String buildParam(String name){
        if(StringUtils.isEmpty(name)){
            return null;
        }else {
            name = name.replaceAll("[$]","");
            return  "$"+name+"$";
        }
    }
}
