package com.deodar.kettle.platform.parameter.controller;

import java.util.List;

import com.deodar.kettle.platform.parameter.domain.RnParameterGroup;
import com.deodar.kettle.platform.parameter.service.IRnParameterGroupService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.deodar.common.annotation.Log;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.common.utils.StringUtils;
import com.deodar.common.core.domain.Ztree;

/**
 * 参数组分类树Controller
 * 
 * @author Administrator
 * @date 2020-04-25
 */
@Controller
@RequestMapping("/kettle/group")
public class RnParameterGroupController extends BaseController {
    private String prefix = "kettle/group";

    @Autowired
    private IRnParameterGroupService rnParameterGroupService;

    @RequiresPermissions("kettle:group:view")
    @GetMapping()
    public String group() {
        return prefix + "/group";
    }

    /**
     * 查询参数组分类树树列表
     */
    @RequiresPermissions("kettle:group:list")
    @PostMapping("/list")
    @ResponseBody
    public List<RnParameterGroup> list(RnParameterGroup rnParameterGroup) {
        List<RnParameterGroup> list = rnParameterGroupService.selectRnParameterGroupList(rnParameterGroup);
        return list;
    }

    /**
     * 导出参数组分类树列表
     */
    @RequiresPermissions("kettle:group:export")
    @Log(title = "参数组分类树", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RnParameterGroup rnParameterGroup) {
        List<RnParameterGroup> list = rnParameterGroupService.selectRnParameterGroupList(rnParameterGroup);
        ExcelUtil<RnParameterGroup> util = new ExcelUtil<RnParameterGroup>(RnParameterGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 新增参数组分类树
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) String id, ModelMap mmap) {
        if (StringUtils.isNotNull(id)) {
            mmap.put("rnParameterGroup", rnParameterGroupService.selectRnParameterGroupById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存参数组分类树
     */
    @RequiresPermissions("kettle:group:add")
    @Log(title = "参数组分类树", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RnParameterGroup rnParameterGroup) {

        return toAjax(rnParameterGroupService.insertRnParameterGroup(rnParameterGroup));
    }

    /**
     * 修改参数组分类树
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        RnParameterGroup rnParameterGroup = rnParameterGroupService.selectRnParameterGroupById(id);
        mmap.put("rnParameterGroup", rnParameterGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存参数组分类树
     */
    @RequiresPermissions("kettle:group:edit")
    @Log(title = "参数组分类树", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RnParameterGroup rnParameterGroup) {
        return toAjax(rnParameterGroupService.updateRnParameterGroup(rnParameterGroup));
    }

    /**
     * 删除
     */
    @RequiresPermissions("kettle:group:remove")
    @Log(title = "参数组分类树", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id) {
        return toAjax(rnParameterGroupService.deleteRnParameterGroupById(id));
    }

    /**
     * 选择参数组分类树树
     */
    @GetMapping(value = { "/selectGroupTree/{id}", "/selectGroupTree/" })
    public String selectGroupTree(@PathVariable(value = "id", required = false) String id, ModelMap mmap) {
        if (StringUtils.isNotNull(id)) {
            mmap.put("rnParameterGroup", rnParameterGroupService.selectRnParameterGroupById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载参数组分类树树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = rnParameterGroupService.selectRnParameterGroupTree();
        return ztrees;
    }
}
