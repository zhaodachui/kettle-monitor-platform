package com.deodar.kettle.platform.parameter.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 参数组对象 rn_parameter_group
 *
 * @author ruoyi
 * @date 2020-04-08
 */
public class RnParameterGroup extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** null */
    @Excel(name = "null")
    private String id;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateTime;

    /** null */
    @Excel(name = "null")
    private String parentId;

    /** null */
    @Excel(name = "null")
    private String name;

    /** null */
    @Excel(name = "备注")
    private String remark;

    /** 父菜单名称 */
    private String parentName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }


    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("lastUpdateTime", getUpdateTime())
            .append("parentId", getParentId())
            .append("name", getName())
            .append("describe", getRemark())
                .append("parentName",getParentName())
            .toString();
    }
}
