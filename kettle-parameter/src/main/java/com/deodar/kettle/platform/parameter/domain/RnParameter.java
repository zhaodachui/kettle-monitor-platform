package com.deodar.kettle.platform.parameter.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.deodar.common.annotation.Excel;
import com.deodar.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 参数组对象 rn_parameter
 * 
 * @author ruoyi
 * @date 2020-04-08
 */
public class RnParameter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    @Excel(name = "null")
    private String id;

    /** null */
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateTime;

    /** null */
    @Excel(name = "null")
    private String parameterGroupId;

    /** null */
    @Excel(name = "null")
    private String name;

    /** null */
    @Excel(name = "null")
    private String value;

    /** null */
    @Excel(name = "null")
    private Long type;

    /** null */
    @Excel(name = "null")
    private String remark;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setParameterGroupId(String parameterGroupId)
    {
        this.parameterGroupId = parameterGroupId;
    }

    public String getParameterGroupId() 
    {
        return parameterGroupId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setValue(String value) 
    {
        this.value = value;
    }

    public String getValue() 
    {
        return value;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("lastUpdateTime", getUpdateTime())
            .append("parameterGroupId", getParameterGroupId())
            .append("name", getName())
            .append("value", getValue())
            .append("type", getType())
            .append("describe", getRemark())
            .toString();
    }
}
