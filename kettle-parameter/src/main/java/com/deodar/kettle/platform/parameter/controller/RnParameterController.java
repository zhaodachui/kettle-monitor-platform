package com.deodar.kettle.platform.parameter.controller;

import java.util.List;

import com.deodar.kettle.platform.parameter.domain.RnParameter;
import com.deodar.kettle.platform.parameter.service.IRnParameterService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.deodar.common.annotation.Log;
import com.deodar.common.enums.BusinessType;
import com.deodar.common.core.controller.BaseController;
import com.deodar.common.core.domain.AjaxResult;
import com.deodar.common.utils.poi.ExcelUtil;
import com.deodar.common.core.page.TableDataInfo;

/**
 * 参数组Controller
 * 
 * @author Administrator
 * @date 2020-04-08
 */
@Controller
@RequestMapping("/kettle/parameter")
public class RnParameterController extends BaseController {
    private String prefix = "kettle/parameter";

    @Autowired
    private IRnParameterService bnParameterService;

    @RequiresPermissions("kettle:parameter:view")
    @GetMapping()
    public String parameter() {
        return prefix + "/parameter";
    }

    /**
     * 查询参数组列表
     */
    @RequiresPermissions("kettle:parameter:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RnParameter rnParameter) {
        startPage();
        List<RnParameter> list = bnParameterService.selectRnParameterList(rnParameter);
        return getDataTable(list);
    }

    /**
     * 导出参数组列表
     */
    @RequiresPermissions("kettle:parameter:export")
    @Log(title = "参数组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RnParameter rnParameter) {
        List<RnParameter> list = bnParameterService.selectRnParameterList(rnParameter);
        ExcelUtil<RnParameter> util = new ExcelUtil<RnParameter>(RnParameter.class);
        return util.exportExcel(list, "parameter");
    }


    /**
     *  新增参数
     * @param id  参数组id
     * @param mmap
     * @return
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id")  String id,ModelMap mmap) {
        RnParameter rnParameter = new RnParameter();
        rnParameter.setParameterGroupId(id);
        mmap.put("rnParameter", rnParameter);
        return prefix + "/add";
    }

    /**
     * 新增保存参数组
     */
    @RequiresPermissions("kettle:parameter:add")
    @Log(title = "参数组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RnParameter rnParameter)
    {
        return toAjax(bnParameterService.insertRnParameter(rnParameter));
    }

    /**
     * 修改参数组
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        RnParameter rnParameter = bnParameterService.selectRnParameterById(id);
        rnParameter.setName(rnParameter.getName().replaceAll("[$]",""));
        mmap.put("rnParameter", rnParameter);
        return prefix + "/edit";
    }

    /**
     * 修改保存参数组
     */
    @RequiresPermissions("kettle:parameter:edit")
    @Log(title = "参数组", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RnParameter rnParameter)
    {
        return toAjax(bnParameterService.updateRnParameter(rnParameter));
    }

    /**
     * 删除参数组
     */
    @RequiresPermissions("kettle:parameter:remove")
    @Log(title = "参数组", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bnParameterService.deleteRnParameterByIds(ids));
    }
}
