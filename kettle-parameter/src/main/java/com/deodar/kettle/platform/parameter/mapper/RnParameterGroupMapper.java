package com.deodar.kettle.platform.parameter.mapper;

import java.util.List;

import com.deodar.kettle.platform.parameter.domain.RnParameterGroup;

/**
 * 参数组Mapper接口
 * 
 * @author Administrator
 * @date 2020-04-08
 */
public interface RnParameterGroupMapper
{
    /**
     * 查询参数组
     * 
     * @param id 参数组ID
     * @return 参数组
     */
    public RnParameterGroup selectRnParameterGroupById(String id);

    /**
     * 查询参数组列表
     * 
     * @param bnParameterGroup 参数组
     * @return 参数组集合
     */
    public List<RnParameterGroup> selectRnParameterGroupList(RnParameterGroup bnParameterGroup);

    /**
     * 新增参数组
     * 
     * @param bnParameterGroup 参数组
     * @return 结果
     */
    public int insertRnParameterGroup(RnParameterGroup bnParameterGroup);

    /**
     * 修改参数组
     * 
     * @param bnParameterGroup 参数组
     * @return 结果
     */
    public int updateRnParameterGroup(RnParameterGroup bnParameterGroup);

    /**
     * 删除参数组
     * 
     * @param id 参数组ID
     * @return 结果
     */
    public int deleteRnParameterGroupById(String id);

    /**
     * 批量删除参数组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRnParameterGroupByIds(String[] ids);
}
