package com.deodar.kettle.platform.parameter.mapper;

import java.util.List;

import com.deodar.kettle.platform.parameter.domain.RnParameter;

/**
 * 参数组Mapper接口
 * 
 * @author Administrator
 * @date 2020-04-08
 */
public interface RnParameterMapper {
    /**
     * 查询参数组
     * 
     * @param id 参数组ID
     * @return 参数组
     */
    public RnParameter selectRnParameterById(String id);

    /**
     * 查询参数组列表
     * 
     * @param bnParameter 参数组
     * @return 参数组集合
     */
    public List<RnParameter> selectRnParameterList(RnParameter bnParameter);

    /**
     * 新增参数组
     * 
     * @param bnParameter 参数组
     * @return 结果
     */
    public int insertRnParameter(RnParameter bnParameter);

    /**
     * 修改参数组
     * 
     * @param bnParameter 参数组
     * @return 结果
     */
    public int updateRnParameter(RnParameter bnParameter);

    /**
     * 删除参数组
     * 
     * @param id 参数组ID
     * @return 结果
     */
    public int deleteRnParameterById(String id);

    /**
     * 批量删除参数组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRnParameterByIds(String[] ids);


    List<RnParameter> selectListByGroupId(String groupId);


}
