package com.deodar.kettle.platform.parameter.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.deodar.kettle.platform.parameter.domain.RnParameterGroup;
import com.deodar.kettle.platform.parameter.mapper.RnParameterGroupMapper;
import com.deodar.kettle.platform.parameter.service.IRnParameterGroupService;
import com.deodar.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.deodar.common.core.text.Convert;

/**
 * 参数组Service业务层处理
 * 
 * @author Administrator
 * @date 2020-04-08
 */
@Service
public class RnParameterGroupServiceImpl implements IRnParameterGroupService {
    @Autowired
    private RnParameterGroupMapper rnParameterGroupMapper;

    /**
     * 查询参数组
     * 
     * @param id 参数组ID
     * @return 参数组
     */
    @Override
    public RnParameterGroup selectRnParameterGroupById(String id) {
        return rnParameterGroupMapper.selectRnParameterGroupById(id);
    }

    /**
     * 查询参数组列表
     * 
     * @param rnParameterGroup 参数组
     * @return 参数组
     */
    @Override
    public List<RnParameterGroup> selectRnParameterGroupList(RnParameterGroup rnParameterGroup) {
        return rnParameterGroupMapper.selectRnParameterGroupList(rnParameterGroup);
    }

    /**
     * 新增参数组
     * 
     * @param rnParameterGroup 参数组
     * @return 结果
     */
    @Override
    public int insertRnParameterGroup(RnParameterGroup rnParameterGroup) {
        rnParameterGroup.setId(String.valueOf(System.currentTimeMillis()));
        rnParameterGroup.setUpdateTime(new Date());
        return rnParameterGroupMapper.insertRnParameterGroup(rnParameterGroup);
    }

    /**
     * 修改参数组
     * 
     * @param rnParameterGroup 参数组
     * @return 结果
     */
    @Override
    public int updateRnParameterGroup(RnParameterGroup rnParameterGroup) {
        rnParameterGroup.setUpdateTime(new Date());
        return rnParameterGroupMapper.updateRnParameterGroup(rnParameterGroup);
    }

    /**
     * 删除参数组对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRnParameterGroupByIds(String ids)
    {
        return rnParameterGroupMapper.deleteRnParameterGroupByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除参数组信息
     * 
     * @param id 参数组ID
     * @return 结果
     */
    @Override
    public int deleteRnParameterGroupById(String id)
    {
        return rnParameterGroupMapper.deleteRnParameterGroupById(id);
    }

    @Override
    public List<Ztree> selectRnParameterGroupTree() {
        List<RnParameterGroup> rnParameterGroupList = rnParameterGroupMapper.selectRnParameterGroupList(new RnParameterGroup());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (RnParameterGroup rnParameterGroup : rnParameterGroupList){
            Ztree ztree = new Ztree();
            ztree.setId(Long.parseLong(rnParameterGroup.getId()));
            ztree.setpId(Long.parseLong(rnParameterGroup.getParentId()));
            ztree.setName(rnParameterGroup.getName());
            ztree.setTitle(rnParameterGroup.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
