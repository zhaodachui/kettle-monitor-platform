--关于在页面打开资源库分类树出错问题，资源库在初始化的时候少了这一列
ALTER TABLE r_directory ADD COLUMN type int(1) NOT NULL DEFAULT 0 ;
--关于在页面打开节点管理出错问题，资源库在初始化的时候少了这一列
ALTER TABLE r_slave ADD COLUMN status int(1) NOT NULL DEFAULT 0 ;
--修改rn_parameter_group 引用的函数类型
DROP FUNCTION IF EXISTS getChild2;
CREATE FUNCTION getChild2 (rootId VARCHAR(1000)) RETURNS VARCHAR(1000) CHARSET utf8
BEGIN
        DECLARE ptemp VARCHAR(1000);
        DECLARE ctemp VARCHAR(1000);
               SET ptemp = '#';
               SET ctemp =CAST(rootId AS CHAR);
               WHILE ctemp is not null DO
                 SET ptemp = CONCAT(ptemp,',',ctemp);
                SELECT GROUP_CONCAT(ID) INTO ctemp FROM rn_parameter_group
                WHERE FIND_IN_SET(parent_id,ctemp)>0;
               END WHILE;
               RETURN ptemp;
END;