package com.deodar.common.utils;


import com.deodar.common.snowflake.SnowflakeIdWorker;

import java.util.UUID;

public class IDUtil {

    /**
     *
     * @return 返回带'-'的uuid
     */
    public static  String getUuid(){
        return UUID.randomUUID().toString();
    }

    /**
     *
     * @return 返回不带'-'的uuid
     */
    public  static  String getUuidNoLine(){
        return UUID.randomUUID().toString().replace("-", "");
    }


    public static SnowflakeIdWorker createSnowflake(long workerId, long datacenterId) {
        return new SnowflakeIdWorker(workerId, datacenterId);
    }

    private static SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(0,0);

    public static String  getSnowflakeId(){
        return   String.valueOf(snowflakeIdWorker.nextId());
    }
}
